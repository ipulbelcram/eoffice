<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    .btn {
        border: none;
        background: #1b85ff;
        color: white;
        padding: 10px 35px;
        border-radius: 5px;
        cursor: pointer;
    }

    .btn:hover {
        background: #006deb;
    }
</style>
<body>
    <p>Notifikasi dari Eoffice klik button berikut untuk masuk</p>
    <div>
        <a href="{{ $link }}">
            <button class="btn">Lihat Surat</button>
        </a>
    </div>
</body>
</html>