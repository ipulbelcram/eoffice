<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisposisiInstruksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disposisi_instruksi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('disposisi_id');
            $table->unsignedBigInteger('instruksi_id');
        });

        Schema::table('disposisi_instruksi', function (Blueprint $table) {
            $table->foreign('disposisi_id')->references('id')->on('disposisi')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('instruksi_id')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposisi_instruksi');
    }
}
