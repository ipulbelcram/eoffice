<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('no_agenda', 191)->nullable();
            $table->string('asal_surat');
            $table->text('perihal')->nullable();
            $table->date('tanggal_surat');
            $table->string('no_surat', 191)->unique();
            $table->unsignedBigInteger('jenis_surat_id')->nullable();
            $table->timestamps();
            $table->softDeletesTz('deleted_at', 0);
        });

        Schema::table('surat', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('jenis_surat_id')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat');
    }
}
