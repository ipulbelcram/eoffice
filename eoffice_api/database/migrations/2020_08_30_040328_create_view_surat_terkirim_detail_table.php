<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewSuratTerkirimDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_surat_terkirim_detail");
        DB::statement("
            CREATE VIEW vw_surat_terkirim_detail as 
            SELECT 
                a.id,
                b.user_id,
                b.no_agenda,
                b.asal_surat,
                b.perihal,
                b.tanggal_surat,
                b.no_surat,
                b.jenis_surat_id,
                a.surat_id,
                a.disposisi_id,
                a.sender_id,
                a.reciver_id,
                a.keterangan,
                a.type,
                a.log_surat_id,
                a.status_message,
                a.created_at
            FROM surat_terkirim a
            LEFT JOIN surat b on a.surat_id = b.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_surat_terkirim_detail');
    }
}
