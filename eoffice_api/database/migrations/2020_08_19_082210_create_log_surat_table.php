<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_surat', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('surat_id');
            $table->enum('log_surat', ['upload_surat', 'kirim_surat', 'kirim_disposisi', 'kirim_informasi', 'proses_disposisi', 'finish_disposisi']);
            $table->timestamps();
            $table->softDeletesTz('deleted_at', 0);
        });

        Schema::table('log_surat', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('surat_id')->references('id')->on('surat')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('surat_terkirim', function(Blueprint $table) {
            $table->foreign('log_surat_id')->references('id')->on('log_surat')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_surat');
    }
}
