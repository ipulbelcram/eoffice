<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 91);
            $table->string('username', 25)->unique();
            $table->string('email', 191)->unique()->nullable();
            $table->string('nip', 25);
            $table->enum('jenis_kelamin', ['laki-laki','perempuan'])->nullable();
            $table->bigInteger('no_hp')->nullable();
            $table->unsignedBigInteger('jabatan_id')->nullable();
            $table->string('jabatan_name');
            $table->unsignedBigInteger('user_level_id');
            $table->string('foto', 191)->nullable();
            $table->boolean('active');
            $table->string('password', 191);
            $table->timestamps();
            $table->softDeletesTz('deleted_at', 0);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('jabatan_id')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_level_id')->references('id')->on('user_levels')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
