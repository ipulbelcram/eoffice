<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTerkirimTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_terkirim', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('surat_id');
            $table->unsignedBigInteger('disposisi_id')->nullable();
            $table->unsignedBigInteger('sender_id');
            $table->unsignedBigInteger('reciver_id')->nullable();
            $table->text('keterangan')->nullable();
            $table->enum('type', ['surat', 'informasi']);
            $table->unsignedBigInteger('log_surat_id');
            $table->enum('status_message', ['sent', 'read']);
            $table->enum('progres_status', ['sent', 'progres', 'finish', 'complete']);
            $table->timestamps();
            $table->softDeletesTz('deleted_at', 0);
        });

        Schema::table('surat_terkirim', function (Blueprint $table) {
           $table->foreign('surat_id')->references('id')->on('surat')->onDelete('cascade')->onUpdate('cascade');
           $table->foreign('disposisi_id')->references('id')->on('disposisi')->onDelete('cascade')->onUpdate('cascade');
           $table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
           $table->foreign('reciver_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_terkirim');
    }
}
