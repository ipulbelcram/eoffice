<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisposisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disposisi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('surat_id');
            $table->unsignedBigInteger('sifat_id');
            $table->timestamps();
            $table->softDeletesTz('deleted_at', 0);
        });

        Schema::table('disposisi', function (Blueprint $table) {
            $table->foreign('surat_id')->references('id')->on('surat')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sifat_id')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposisi');
    }
}
