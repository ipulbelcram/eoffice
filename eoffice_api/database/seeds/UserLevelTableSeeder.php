<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_levels')->insert([
            'id' => 1,
            'user_type' => 'administrator'
        ]);

        DB::table('user_levels')->insert([
            'id' => 10,
            'user_type' => 'super admin'
        ]);

        DB::table('user_levels')->insert([
            'id' => 101,
            'user_type' => 'operator',
        ]);
    }
}
