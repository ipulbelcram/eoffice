<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ParamsTableSeeder');
        $this->call('UserLevelTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('UserGroupTableSeeder');
    }
}
