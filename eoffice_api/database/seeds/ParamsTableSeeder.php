<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('params')->insert([
            'id' => 1,
            'category_param' => 'jenis_surat',
            'param' => 'Memorandum',
            'active' => true,
            'order' => 1,
        ]);

        DB::table('params')->insert([
            'id' => 2,
            'category_param' => 'jenis_surat',
            'param' => 'Permohonan Narasumber',
            'active' => true,
            'order' => 2,
        ]);

        DB::table('params')->insert([
            'id' => 3,
            'category_param' => 'jenis_surat',
            'param' => 'Undangan Rapat/Acara',
            'active' => true,
            'order' => 3,
        ]);

        DB::table('params')->insert([
            'id' => 4,
            'category_param' => 'jenis_surat',
            'param' => 'Proposal Kegiatan',
            'active' => true,
            'order' => 4,
        ]);

        DB::table('params')->insert([
            'id' => 5,
            'category_param' => 'jenis_surat',
            'param' => 'Surat Edaran/Pemberitahuan',
            'active' => true,
            'order' => 5,
        ]);

        DB::table('params')->insert([
            'id' => 6,
            'category_param' => 'sifat_disposisi',
            'param' => 'Segera',
            'active' => true,
            'order' => 1,
        ]);

        DB::table('params')->insert([
            'id' => 7,
            'category_param' => 'sifat_disposisi',
            'param' => 'Penting',
            'active' => true,
            'order' => 2,
        ]);

        DB::table('params')->insert([
            'id' => 8,
            'category_param' => 'sifat_disposisi',
            'param' => 'Rahasia',
            'active' => true,
            'order' => 3,
        ]);

        DB::table('params')->insert([
            'id' => 9,
            'category_param' => 'sifat_disposisi',
            'param' => 'Rutin',
            'active' => true,
            'order' => 4,
        ]);

        DB::table('params')->insert([
            'id' => 10,
            'category_param' => 'sifat_disposisi',
            'param' => 'Biasa',
            'active' => true,
            'order' => 5,
        ]);

        DB::table('params')->insert([
            'id' => 11,
            'category_param' => 'instruksi',
            'param' => 'Dokumentasi File',
            'active' => true,
            'order' => 1,
        ]);

        DB::table('params')->insert([
            'id' => 12,
            'category_param' => 'instruksi',
            'param' => 'Mohon hadir Mewakili Saya',
            'active' => true,
            'order' => 2,
        ]);

        DB::table('params')->insert([
            'id' => 13,
            'category_param' => 'instruksi',
            'param' => 'Membicarakan Dengan Saya',
            'active' => true,
            'order' => 3,
        ]);

        DB::table('params')->insert([
            'id' => 14,
            'category_param' => 'instruksi',
            'param' => 'Membuat Jawaban/Tanggapan',
            'active' => true,
            'order' => 4,
        ]);

        DB::table('params')->insert([
            'id' => 15,
            'category_param' => 'instruksi',
            'param' => 'Ikut Hadir',
            'active' => true,
            'order' => 5,
        ]);

        DB::table('params')->insert([
            'id' => 16,
            'category_param' => 'instruksi',
            'param' => 'Memonitor',
            'active' => true,
            'order' => 6,
        ]);

        DB::table('params')->insert([
            'id' => 17,
            'category_param' => 'instruksi',
            'param' => 'Menyiapkan Konsep',
            'active' => true,
            'order' => 7,
        ]);

        DB::table('params')->insert([
            'id' => 18,
            'category_param' => 'instruksi',
            'param' => 'DIketahui/Sebagai Informasi',
            'active' => true,
            'order' => 8,
        ]);

        DB::table('params')->insert([
            'id' => 19,
            'category_param' => 'instruksi',
            'param' => 'Mempelajari dan Memberikan Saran',
            'active' => true,
            'order' => 9,
        ]);

        DB::table('params')->insert([
            'id' => 20,
            'category_param' => 'instruksi',
            'param' => 'Melaksanakan/Menindaklanjuti',
            'active' => true,
            'order' => 10,
        ]);

        DB::table('params')->insert([
            'id' => 21,
            'category_param' => 'instruksi',
            'param' => 'Memproses Sesuai Prosedur',
            'active' => true,
            'order' => 11,
        ]);

        DB::table('params')->insert([
            'id' => 22,
            'category_param' => 'instruksi',
            'param' => 'Menyelesaikan Sebelum Batas Waktu',
            'active' => true,
            'order' => 12,
        ]);

        DB::table('params')->insert([
            'id' => 23,
            'category_param' => 'instruksi',
            'param' => 'Mengkoordinasikan',
            'active' => true,
            'order' => 13,
        ]);

        DB::table('params')->insert([
            'id' => 24,
            'category_param' => 'jabatan',
            'param' => 'Deputi',
            'active' => true,
            'order' => 1,
        ]);

        DB::table('params')->insert([
            'id' => 25,
            'category_param' => 'jabatan',
            'param' => 'Sesdep',
            'active' => true,
            'order' => 2,
        ]);

        DB::table('params')->insert([
            'id' => 26,
            'category_param' => 'jabatan',
            'param' => 'Asdep',
            'active' => true,
            'order' => 2,
        ]);

        DB::table('params')->insert([
            'id' => 27,
            'category_param' => 'jabatan',
            'param' => 'Kabid',
            'active' => true,
            'order' => 3,
        ]);

        DB::table('params')->insert([
            'id' => 28,
            'category_param' => 'jabatan',
            'param' => 'Kasubid',
            'active' => true,
            'order' => 4,
        ]);

        DB::table('params')->insert([
            'id' => 29,
            'category_param' => 'jabatan',
            'param' => 'Staff',
            'active' => false,
            'order' => 5,
        ]);
    }
}
