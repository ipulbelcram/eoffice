<?php

use App\Models\UserGroup;
use Illuminate\Database\Seeder;

class UserGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usergroup::create([
            'id' => 1,
            'user_id' => 4,
            'atasan_id' => NULL,
            'asdep_id' => NULL
        ]);

        Usergroup::create([
            'id' => 2,
            'user_id' => 5,
            'atasan_id' => 4,
            'asdep_id' => NULL
        ]);

        Usergroup::create([
            'id' => 3,
            'user_id' => 6,
            'atasan_id' => 4,
            'asdep_id' => NULL
        ]);

        Usergroup::create([
            'id' => 4,
            'user_id' => 7,
            'atasan_id' => 4,
            'asdep_id' => NULL
        ]);

        Usergroup::create([
            'id' => 5,
            'user_id' => 8,
            'atasan_id' => 4,
            'asdep_id' => NULL
        ]);

        Usergroup::create([
            'id' => 6,
            'user_id' => 9,
            'atasan_id' => 4,
            'asdep_id' => NULL
        ]);

        Usergroup::create([
            'id' => 7,
            'user_id' => 10,
            'atasan_id' => 9,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 8,
            'user_id' => 12,
            'atasan_id' => 8,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 9,
            'user_id' => 13,
            'atasan_id' => 8,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 10,
            'user_id' => 14,
            'atasan_id' => 9,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 11,
            'user_id' => 15,
            'atasan_id' => 13,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 12,
            'user_id' => 16,
            'atasan_id' => 6,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 13,
            'user_id' => 17,
            'atasan_id' => 7,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 14,
            'user_id' => 18,
            'atasan_id' => 7,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 15,
            'user_id' => 19,
            'atasan_id' => 6,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 16,
            'user_id' => 20,
            'atasan_id' => 7,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 17,
            'user_id' => 21,
            'atasan_id' => 8,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 18,
            'user_id' => 23,
            'atasan_id' => 6,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 19,
            'user_id' => 25,
            'atasan_id' => 17,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 20,
            'user_id' => 26,
            'atasan_id' => 13,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 21,
            'user_id' => 27,
            'atasan_id' => 10,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 22,
            'user_id' => 28,
            'atasan_id' => 23,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 23,
            'user_id' => 29,
            'atasan_id' => 24,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 24,
            'user_id' => 30,
            'atasan_id' => 22,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 25,
            'user_id' => 31,
            'atasan_id' => 14,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 26,
            'user_id' => 32,
            'atasan_id' => 19,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 27,
            'user_id' => 33,
            'atasan_id' => 21,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 28,
            'user_id' => 34,
            'atasan_id' => 22,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 29,
            'user_id' => 35,
            'atasan_id' => 11,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 30,
            'user_id' => 36,
            'atasan_id' => 64,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 31,
            'user_id' => 37,
            'atasan_id' => 19,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 32,
            'user_id' => 38,
            'atasan_id' => 12,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 33,
            'user_id' => 39,
            'atasan_id' => 17,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 34,
            'user_id' => 40,
            'atasan_id' => 16,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 35,
            'user_id' => 41,
            'atasan_id' => 18,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 36,
            'user_id' => 42,
            'atasan_id' => 20,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 37,
            'user_id' => 43,
            'atasan_id' => 20,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 38,
            'user_id' => 44,
            'atasan_id' => 14,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 39,
            'user_id' => 45,
            'atasan_id' => 11,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 40,
            'user_id' => 46,
            'atasan_id' => 12,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 41,
            'user_id' => 47,
            'atasan_id' => 10,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 42,
            'user_id' => 48,
            'atasan_id' => 24,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 43,
            'user_id' => 49,
            'atasan_id' => 18,
            'asdep_id' => 7
        ]);

        Usergroup::create([
            'id' => 44,
            'user_id' => 50,
            'atasan_id' => 23,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 45,
            'user_id' => 52,
            'atasan_id' => 16,
            'asdep_id' => 6
        ]);

        Usergroup::create([
            'id' => 46,
            'user_id' => 53,
            'atasan_id' => 21,
            'asdep_id' => 8
        ]);

        Usergroup::create([
            'id' => 47,
            'user_id' => 56,
            'atasan_id' => 5,
            'asdep_id' => 5
        ]);

        Usergroup::create([
            'id' => 48,
            'user_id' => 57,
            'atasan_id' => 5,
            'asdep_id' => 5
        ]);

        Usergroup::create([
            'id' => 49,
            'user_id' => 59,
            'atasan_id' => 56,
            'asdep_id' => 5
        ]);

        Usergroup::create([
            'id' => 50,
            'user_id' => 60,
            'atasan_id' => 56,
            'asdep_id' => 5
        ]);

        Usergroup::create([
            'id' => 51,
            'user_id' => 61,
            'atasan_id' => 57,
            'asdep_id' => 5
        ]);

        Usergroup::create([
            'id' => 52,
            'user_id' => 62,
            'atasan_id' => 57,
            'asdep_id' => 5
        ]);

        Usergroup::create([
            'id' => 53,
            'user_id' => 11,
            'atasan_id' => 63,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 54,
            'user_id' => 22,
            'atasan_id' => 63,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 55,
            'user_id' => 24,
            'atasan_id' => 63,
            'asdep_id' => 63
        ]);

        Usergroup::create([
            'id' => 56,
            'user_id' => 51,
            'atasan_id' => 64,
            'asdep_id' => 9
        ]);

        Usergroup::create([
            'id' => 57,
            'user_id' => 63,
            'atasan_id' => 4,
            'asdep_id' => NULL
        ]);
    }
}
