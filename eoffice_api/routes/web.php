<?php

use App\Models\Disposisi;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/total_document', ['as' => 'total_document', 'uses' => 'TotalController']);
$router->post('/file_name_cek', ['as' => 'file_name_cek', 'uses' => 'Surat\FileController@fileNameCek']);

// Auth Route
$router->group(['prefix' => 'auth', 'namespace' => 'Auth'], function () use ($router) {
    $router->post('/login', 'LoginController');
    $router->get('/user', 'UserController');
    $router->patch('/password_update', 'PasswordController');
    $router->post('/logout', 'LogoutController');
});

// User Route
$router->group(['prefix' => 'user', 'namespace' => 'User'], function () use ($router) {
    $router->get('/', 'UserController@allUser');
    $router->post('/create', 'CreateUserController');
    $router->patch('/update/{id}', 'UpdateController');
    $router->patch('/update_foto/{id}', 'UpdateFotoController');
    $router->get('/detail/{id}', 'UserController@userDetail');
    $router->get('/user_reciver', 'UserController@userReciver');
    $router->post('/reset_password', 'ResetPasswordController');
    $router->get('/user_asdep', 'UserController@getAsdep');
    $router->get('/user_atasan/{jabatan_id}', 'UserController@getAtasan');
});

// Surat Route
$router->group(['prefix' => 'surat', 'namespace' => 'Surat'], function () use ($router) {
    $router->post('/create', 'CreateController');
    $router->post('/kirim_surat/{surat_id}', 'KirimSuratController');
    $router->get('/surat_masuk[/{id_user}]', 'SuratMasukController');
    $router->get('/surat_keluar', 'SuratKeluarController');
    $router->get('/detail/{surat_id}', 'DetailController');
    $router->patch('/status_message/{surat_terkirim_id}', 'StatusMessageController');
    $router->post('/filter_by_jenis_surat', 'FilterController@filterByJenisSurat');
    // $router->post('/search_by_no_agenda', 'SearchController@serachByNoAgenda');
    $router->post('/search_by_no_surat', 'SearchController@searchByNoSurat');
    $router->post('/progres_surat/progres/{surat_terkirim_id}', 'ProgresSuratController@progres');
    $router->post('/progres_surat/finish/{surat_terkirim_id}', 'ProgresSuratController@finish');
    $router->get('/surat_terkirim/{surat_terkirim_id}', 'SuratTerkirimController');
    $router->patch('/edit_surat/{surat_id}', 'EditController');
    $router->delete('/delete_surat/{surat_id}', 'DeleteController');
});

// Disposisi Route
$router->group(['prefix' => 'disposisi', 'namespace' => 'Disposisi'], function () use ($router) {
    $router->post('/kirim_disposisi/{surat_id}/{surat_terkirim_id}', 'KirimDisposisiController');
    $router->post('/teruskan_disposisi/{disposisi_id}/{surat_terkirim_id}', 'TeruskanDisposisiController');
    $router->get('/disposisi_masuk', 'DisposisiMasukController');
    $router->get('/disposisi_keluar', 'DisposisiKeluarController');
    $router->get('/detail/{disposisi_id}', 'DetailController');
});

// Informasi Route
$router->group(['prefix' => 'informasi', 'namespace' => 'Informasi'], function() use ($router) {
    $router->post('/create', 'CreateController');
    $router->post('/teruskan_informasi/{informasi_id}', 'TeruskanInformasiController');
    $router->get('/detail/{informasi_id}', 'DetailController');
    $router->get('/informasi_masuk', 'InformasiMasukController');
    $router->get('/informasi_keluar', 'InformasiKeluarController');
});

$router->group(['prefix' => 'log_history', 'namespace' => 'History'], function () use ($router) {
    $router->get('detail/{log_surat_id}', 'DetailController');
});

//Route Param
$router->group(['prefix' => 'param', 'namespace' => 'Param'], function () use ($router) {
    $router->get('jenis_surat', 'GetParamController@getJenisSurat');
    $router->get('sifat_disposisi', 'GetParamController@getSifatDisposisi');
    $router->get('instruksi', 'GetParamController@getInstruksi');
    $router->get('jabatan', 'GetParamController@getJabatan');
    $router->get('user_level', 'GetParamController@getUserLevel');
});