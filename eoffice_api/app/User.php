<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'nip', 'jenis_kelamin', 'no_hp', 'jabatan_id', 'jabatan_name','user_level_id', 'foto', 'active', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Models\Param', 'jabatan_id');
    }

    public function surat()
    {
        return $this->hasMany('App\Models\Surat', 'surat_id');
    }

    public function log_surat()
    {
        return $this->hasMany('App\Models\LogSurat', 'user_id');
    }

    public function user_sender()
    {
        return $this->hasMany('App\Models\SuratTerkirim', 'sender_id');
    }

    public function user_reciver()
    {
        return $this->hasMany('App\Models\SuratTerkirim', 'reciver_id');
    }

    public function user_groups()
    {
        return $this->hasOne('App\Models\UserGroup', 'user_id');
    }
}
