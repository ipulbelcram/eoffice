<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
    protected $table = 'params';

    protected $filable = [
        'category_param', 'param', 'active', 'order'
    ];

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\User', 'jabatan_id');
    }

    public function surat()
    {
        return $this->hasMany('App\Models\Surat', 'jenis_surat_id');
    }

    public function instruksi()
    {
        return $this->belongsToMany('App\Models\Disposisi', 'disposisi_instruksi', 'instruksi_id', 'disposisi_id')->withPivot('id');
    }

    public function sifat()
    {
        return $this->hasMany('App\Models\Disposisi', 'sifat_id');
    }
}