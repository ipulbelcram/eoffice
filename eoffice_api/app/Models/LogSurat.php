<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogSurat extends Model
{
    protected $table = 'log_surat';

    protected $fillable = [
        'user_id', 'surat_id', 'log_surat'
    ];

    public function surat()
    {
        return $this->belongsTo('App\Models\Surat', 'surat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function surat_terkirim()
    {
        return $this->hasMany('App\Models\SuratTerkirim', 'log_surat_id');
    }
}