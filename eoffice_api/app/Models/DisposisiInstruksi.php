<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DisposisiInstruksi extends Model
{
    protected $table = 'disposisi_instruksi';

    protected $fillable = [
        'disposisi_id', 'instruksi_id'
    ];

    public $timestamps = false;
}