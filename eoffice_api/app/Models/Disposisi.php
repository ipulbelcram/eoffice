<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disposisi extends Model
{
    protected $table = 'disposisi';

    protected $fillable = [
        'surat_id', 'sifat_id'
    ];

    public function surat()
    {
        return $this->belongsTo('App\Models\Surat', 'surat_id');
    }

    public function instruksi()
    {
        return $this->belongsToMany('App\Models\Param', 'disposisi_instruksi', 'disposisi_id', 'instruksi_id')->withPivot('id');
    }

    public function surat_terkirim()
    {
        return $this->hasMany('App\Models\SuratTerkirim', 'disposisi_id');
    }

    public function sifat()
    {
        return $this->belongsTo('App\Models\Param', 'sifat_id');
    }
}