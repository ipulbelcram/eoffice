<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    protected $table = 'surat';

    protected $fillable = [
        'user_id', 'no_agenda', 'asal_surat', 'perihal', 'tanggal_surat', 'no_surat', 'jenis_surat_id'
    ];

    public function lampiran_surat() {
        return $this->hasMany('App\Models\LampiranSurat', 'surat_id');
    }

    public function jenis_surat()
    {
        return $this->belongsTo('App\Models\Param', 'jenis_surat_id');
    }

    public function surat_terkirim()
    {
        return $this->hasMany('App\Models\SuratTerkirim', 'surat_id');
    }

    public function log_surat()
    {
        return $this->hasMany('App\Models\LogSurat', 'surat_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function disposisi()
    {
        return $this->hasMany('App\Models\Disposisi', 'surat_id');
    }
}