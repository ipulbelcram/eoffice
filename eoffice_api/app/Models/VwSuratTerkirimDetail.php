<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VwSuratTerkirimDetail extends Model
{
    protected $table = 'vw_surat_terkirim_detail';

    public function surat()
    {
        return $this->belongsTo('App\Models\Surat', 'surat_id');
    }

    public function disposisi()
    {
        return $this->belongsTo('App\Models\Disposisi', 'disposisi_id');
    }

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function reciver()
    {
        return $this->belongsTo('App\User', 'reciver_id');
    }

    public function log_surat()
    {
        return $this->belongsTo('App\Models\LogSurat', 'log_surat_id');
    }
}