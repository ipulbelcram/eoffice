<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LampiranSurat extends Model
{
    protected $table = 'lampiran_surat';

    protected $fillable = [
        'surat_id', 'file', 'file_name'
    ];

    public $timestamps = false;

    public function surat() {
        return $this->belongsTo('App\Models\Surat', 'surat_id');
    }
}