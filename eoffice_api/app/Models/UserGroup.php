<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'user_groups';

    protected $fillable = [
        'user_id', 'atasan_id', 'atasan_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'user_id');
    }
}