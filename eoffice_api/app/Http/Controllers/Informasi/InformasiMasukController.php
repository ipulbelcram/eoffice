<?php

namespace App\Http\Controllers\Informasi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratResource;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class InformasiMasukController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $user_id = $request->user()->id;
        $informasiMasuk = SuratTerkirim::where([['reciver_id', $user_id], ['disposisi_id', null], ['type', 'informasi']]);
        
        $totalInformasiMasuk = $informasiMasuk->count();
        $getInformasiMasuk = $informasiMasuk->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'total_informasi_masuk' => $totalInformasiMasuk,
            'data' => ListSuratResource::collection($getInformasiMasuk),
        ]);

    }
}
