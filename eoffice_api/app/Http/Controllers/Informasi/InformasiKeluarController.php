<?php

namespace App\Http\Controllers\Informasi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratResource;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class InformasiKeluarController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $user_id = $request->user()->id;
        $informasiKeluar = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', '!=', null], ['type', 'informasi']])->groupBy('log_surat_id')->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'total_informasi_keluar' => count($informasiKeluar),
            'data' => ListSuratResource::collection($informasiKeluar),
        ]);

    }
}
