<?php

namespace App\Http\Controllers\Informasi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class CreateController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        // Validation
        $this->validate($request, [
            'asal_surat' => 'required',
            'tanggal_surat' => 'required|date',
            'no_surat' => 'required|unique:surat,no_surat',
            'file' => 'required|array',
            'file.*' => 'required|url',
            'reciver_id' => 'required|array',
            'reciver_id.*' => 'required|numeric|exists:users,id',
        ]);

        // Create Variable
        $input = $request->all();
        $file = $request->input('file');
        $reciver_id = $request->input('reciver_id');
        $input['user_id'] = $request->user()->id;

        // Create Surat
        $surat = Surat::create($input);

        // Create File
        $files = [];
        foreach($file as $key => $value) {
            $fileInput = [
                'file' => $value,
            ];
            $files[] = $fileInput;
        }
        $surat->lampiran_surat()->createMany($files);

        // Create Send Message and Log Message
        $logSuratInput['user_id'] = $request->user()->id;
        $logSuratInput['log_surat'] = 'kirim_informasi';
        $log_surat = $surat->log_surat()->create($logSuratInput);

        foreach($reciver_id as $key => $value) {
            $reciver = [
                'sender_id' => $request->user()->id,
                'reciver_id' => $value,
                'type' => 'informasi',
                'log_surat_id' => $log_surat->id,
            ];
            $surat_terkirim = $surat->surat_terkirim()->create($reciver);

            $user_reciver = User::find($value);
            if(!empty($user_reciver->email)) {
                $link = 'http://eofficekukm.siwira.id/informasi-masuk/'.$surat->id.'/'.$surat_terkirim->id;
                Mail::send('mail', ['data' => $surat, 'link' => $link], function ($message) use ($user_reciver) {
                    $message->subject('Notifikasi dari E-Office');
                    $message->from('noreplay@eofficekukm.siwira.id', 'E-Office KUKM');
                    $message->to($user_reciver->email);
                });
            }
        }

        return new SuratDetailResource($surat);
    }
}
