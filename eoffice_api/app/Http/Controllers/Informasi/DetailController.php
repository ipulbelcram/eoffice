<?php

namespace App\Http\Controllers\Informasi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;

class DetailController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke($informasi_id)
    {
        $informasi = Surat::find($informasi_id);

        if($informasi) {
            return new SuratDetailResource($informasi);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
