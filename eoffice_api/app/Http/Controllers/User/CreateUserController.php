<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDetailResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CreateUserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $jabatan = $request->jabatan_id;
        if($jabatan == 24 || $jabatan == 25 || $jabatan == 26) {
            $asdep = [
                'nullable',
                Rule::exists('users', 'id')->where(function($query) {
                    $query->whereIn('jabatan_id', [25, 26]);
                }),
            ];
        } else {
            $asdep = [
                'required',
                Rule::exists('users', 'id')->where(function($query) {
                    $query->whereIn('jabatan_id', [25, 26]);
                }),
            ];
        }
        if($jabatan == 24) {
            $atasan = ['nullable', 'exists:users,id'];
        } else {
            $atasan = ['required', 'exists:users,id'];
        }
        $this->validate($request, [
            'name' => ['required', 'string'],
            'username' => ['required', 'unique:users,username'],
            'email' => ['required', 'email', 'unique:users,email'],
            'nip' => ['required','string','digits:16','unique:users,nip'],
            'jenis_kelamin' => ['required', 'in:laki-laki,perempuan'],
            'no_hp' => ['nullable', 'numerinc', 'digits_between:8,15'],
            'jabatan_id' => [
                'required',
                Rule::exists('params', 'id')->where(function($query) {
                    $query->where('category_param', 'jabatan');
                })
            ],
            'jabatan_name' => ['required', 'string'],
            'user_level_id' => ['required', 'numeric', 'exists:user_levels,id'],
            'foto' =>  ['nullable', 'url'],
            'atasan_id' => $atasan,
            'asdep_id' => $asdep,
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        $user = User::create($input);
        $user->user_groups()->create([
            'atasan_id' => $request->atasan_id,
            'asdep_id' => $request->asdep_id,
        ]);
        return new UserDetailResource($user);
    }
}
