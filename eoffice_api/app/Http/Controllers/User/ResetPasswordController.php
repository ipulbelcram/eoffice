<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
        ]);
        $user_id = $request->user_id;
        $password = $request->password;
        
        $user = User::find($user_id);
        if($user) {
            $input['password'] = Hash::make($password);
            $user->update($input);
            return response()->json(['success' => 'you are change your password'], 200);
        } else {
            return response()->json([
                'message' => 'data not found'
            ], 200);
        }
    }
}
