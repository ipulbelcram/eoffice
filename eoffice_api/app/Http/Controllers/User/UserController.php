<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDetailResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function userDetail(Request $request, $id)
    {
        $user = User::find($id);
        if($user) {
            return new UserDetailResource($user);
        } else {
            return response()->json(['data not found'], 404);
        }
    }

    public function userReciver(Request $request) 
    {
        $user_id = $request->user()->id;
        $jabatan_id = $request->user()->jabatan_id;
        $user_join = User::leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')->select('users.*', 'user_groups.atasan_id', 'user_groups.user_id');
        $user_data = $user_join->where('users.id', $user_id)->first();
        $atasan_id = $user_data->atasan_id;
        if(!empty($user_data->user_groups->asdep_id)) {
            $asdep_id = $user_data->user_groups->asdep_id;
        }
        if($jabatan_id == 24) {
            $data_user = User::leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')
                        ->select('users.*', 'user_groups.atasan_id', 'user_groups.user_id')
                        ->whereIn('users.jabatan_id', [24, 30, 32])->where([['users.id', '!=', $user_id], ['active', 1]])
                        ->orWhere([['atasan_id', $user_id], ['active', 1]])->get();
        } else if($jabatan_id == 25 || $jabatan_id == 26) {
            $data_user = User::leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')
                        ->select('users.*', 'user_groups.atasan_id', 'user_groups.user_id')
                        ->whereIn('users.jabatan_id', [25, 26])->where([['users.id', '!=', $user_id], ['active', 1]])
                        ->orWhere([['atasan_id', $user_id], ['active', 1]])->get();
        } else if($jabatan_id == 27 || $jabatan_id == 30) {
            $data_user = User::leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')
                        ->select('users.*', 'user_groups.atasan_id', 'user_groups.user_id')
                        ->where([['users.jabatan_id', $jabatan_id], ['users.id', '!=', $user_id], ['atasan_id', $atasan_id], ['active', 1]])
                        ->orWhere([['atasan_id', $user_id], ['active', 1]])->get();
        } else if($jabatan_id == 28 || $jabatan_id == 31) {
            $data_user = User::leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')
                        ->select('users.*', 'user_groups.atasan_id', 'user_groups.user_id')
                        ->where([['users.jabatan_id', $jabatan_id], ['users.id', '!=', $user_id], ['asdep_id', $asdep_id], ['active', 1]])->get();
        } else if($jabatan_id == 32) {
            $data_user = User::leftJoin('user_groups', 'users.id', '=', 'user_groups.user_id')
                        ->select('users.*', 'user_groups.atasan_id', 'user_groups.user_id')
                        ->where([['users.jabatan_id', 31], ['users.id', '!=', $user_id], ['asdep_id', $asdep_id], ['active', 1]])->get();
        }
        return UserResource::collection($data_user);
    }

    public function allUser()
    {
        $user = User::where('user_level_id', 101)->paginate(15);
        return UserResource::collection($user);
    }

    public function getAsdep()
    {
        $user = User::whereIn('jabatan_id', [25,26])->get();
        return UserResource::collection($user);
    }

    public function getAtasan($jabatan_id)
    {
        if( $jabatan_id == 31 ) {
            $atasan_id = 30;
        } else if($jabatan_id == 30) {
            $atasan_id = 25;
        } else if($jabatan_id == 28) {
            $atasan_id = 27;
        } else if($jabatan_id == 27) {
            $atasan_id = 26;
        } else if($jabatan_id == 26 || $jabatan_id == 25) {
            $atasan_id = 24;
        } else {
            $atasan_id = 0;
        }

        $user = User::where([['jabatan_id', $atasan_id], ['active', 1]])->get();
        return UserResource::collection($user);
    }
}
