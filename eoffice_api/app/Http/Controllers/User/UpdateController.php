<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDetailResource;
use App\User;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $id)
    {
        $user = User::find($id);
        if($user) {
            $this->validate($request, [
                'email' => 'nullable|email|unique:users,email,'.$id,
                'name' => 'required',
                'nip' => 'required|string|min:8|unique:users,nip,'.$id,
                'jenis_kelamin' => 'nullable|in:laki-laki,perempuan',
                'no_hp' => 'nullable|numeric|digits_between:8,15',
            ]);

            $input['email'] = !empty($request->email) ? $request->email : NULL;
            $input['name'] = $request->name;
            $input['nip'] = $request->nip;
            $input['jenis_kelamin'] = $request->jenis_kelamin;
            $input['no_hp'] = $request->no_hp;
            $user->update($input);

            return new UserDetailResource($user);

        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
