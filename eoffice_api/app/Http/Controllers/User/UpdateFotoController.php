<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDetailResource;
use App\User;
use Illuminate\Http\Request;

class UpdateFotoController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $id)
    {
        $user = User::find($id);
        if($user) {
            $this->validate($request, [
                'foto' => 'required|url'
            ]);

            $input['foto'] = $request->input('foto');
            $user->update($input);

            return new UserDetailResource($user);

        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
