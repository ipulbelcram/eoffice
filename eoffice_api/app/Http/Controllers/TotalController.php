<?php
namespace App\Http\Controllers;

use App\Models\Surat;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class TotalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $user_id = $request->user()->id;

        $suratMasuk = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', null], ['type', 'surat']])->orWhere([['reciver_id', $user_id], ['disposisi_id', null], ['type', 'surat']])->count();
        $suratKeluar = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', '!=', null], ['type', 'surat']])->groupBy('log_surat_id')->get();
        $disposisiMasuk = SuratTerkirim::where([['reciver_id', $user_id], ['disposisi_id', '!=', null], ['type', 'surat']])->count();
        $disposisiKeluar = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', '!=', null], ['type', 'surat']])->groupBy('log_surat_id')->get();
        $informasiMasuk = SuratTerkirim::where([['reciver_id', $user_id], ['disposisi_id', null], ['type', 'informasi']])->count();
        $informasiKeluar = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', '!=', null], ['type', 'informasi']])->groupBy('log_surat_id')->get();
        
        return response()->json(['data' => [
            'surat_masuk' => $suratMasuk,
            'surat_keluar' => count($suratKeluar),
            'disposisi_masuk' => $disposisiMasuk,
            'disposisi_keluar' => count($disposisiKeluar),
            'informasi_masuk' => $informasiMasuk,
            'informasi_keluar' => count($informasiKeluar),
        ]], 200);
    }
}
