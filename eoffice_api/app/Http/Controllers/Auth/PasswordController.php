<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
        ]);

        $user = User::find($request->user()->id);
        $currentPassword = $user->password;
        $old_password = $request->input('old_password');

        if(Hash::check($old_password, $currentPassword)) {
            $input['password'] = Hash::make($request->input('password'));
            $user->update($input);
            return response()->json(['success' => 'you are change your password'], 200);
        } else {
            return response()->json(['old_password' => 'make sure your fill your current password '], 422);
        }
    }
}
