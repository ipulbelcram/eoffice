<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|string|min:8',
        ]);

        $token = app('auth')->attempt(['username' => $request->username, 'password' => $request->password, 'active' => true]);
        $token2 =  app('auth')->attempt(['email' => $request->username, 'password' => $request->password, 'active' => true]);
        if($token) {
            return response()->json(['token' => $token]);
        } else if($token2) {
            return response()->json(['token' => $token2]);
        } else {
            return response()->json('data not found', 404);
        }
    }
}
