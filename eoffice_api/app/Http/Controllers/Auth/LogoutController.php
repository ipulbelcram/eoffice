<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{

    public function __invoke(Request $request)
    {
        $logout = auth()->logout();
        return response()->json(['logout success'], 200);
    }
}
