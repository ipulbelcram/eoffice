<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDetailResource;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller {
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function __invoke(Request $request)
    {
        $user = User::find($request->user()->id);
        if($user) {
            return new UserDetailResource($user);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}