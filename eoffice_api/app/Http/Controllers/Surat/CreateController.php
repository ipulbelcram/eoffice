<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class CreateController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        // Validation 
        $this->validate($request, [
            'asal_surat' => 'required',
            'perihal' => 'required',
            'tanggal_surat' => 'required|date',
            'no_surat' => 'required|unique:surat,no_surat',
            'jenis_surat_id' => [
                'required', 
                'numeric', 
                Rule::exists('params', 'id')->where(function ($query) {
                    $query->where('category_param', 'jenis_surat');
                }),
            ],
            'file' => 'required|array',
            'file.*.file' => 'required|url',
            'file.*.file_name' => 'required|string',
            'reciver_id' => 'nullable|array',
            'reciver_id.*' => 'required|numeric|exists:users,id',
        ]);

        // Create Variable
        $input = $request->all();
        $file = $request->input('file');
        $reciver_id = $request->input('reciver_id');
        $no_agenda = Surat::where('user_id', $request->user()->id)->orderBy('no_agenda', 'DESC')->first();
        $input['user_id'] = $request->user()->id;
        if(!empty($no_agenda)) {
            $input['no_agenda'] = $no_agenda->no_agenda + 1;
        } else {
            $input['no_agenda'] = 1;
        }

        // Create Surat
        $surat = Surat::create($input);

        // Create File
        $files = [];
        foreach($file as $key => $value) {
            $fileInput = [
                'file' => $value['file'],
                'file_name' => $value['file_name'],
            ];
            $files[] = $fileInput;
        }
        $surat->lampiran_surat()->createMany($files);

        // Create Send Message and Log Message
        $logSuratInput['user_id'] = $request->user()->id;
        if(!empty($reciver_id)) {
            $logSuratInput['log_surat'] = 'kirim_surat';
            $log_surat = $surat->log_surat()->create($logSuratInput);

            
            foreach($reciver_id as $key => $value) {
                $reciver = [
                    'sender_id' => $request->user()->id,
                    'reciver_id' => $value,
                    'log_surat_id' => $log_surat->id,
                    'progres_status' => 'complete',
                ];
                $surat_terkirim = $surat->surat_terkirim()->create($reciver);
                $user_reciver = User::find($value);
                if(!empty($user_reciver->email)) {
                    $link = "http://eofficekukm.siwira.id/surat-masuk/".$surat->id."/".$surat_terkirim->id;
                    Mail::send('mail', ['data' => $surat, 'link' => $link], function ($message) use ($user_reciver) {
                        $message->subject('Notifikasi dari E-Office');
                        $message->from('noreplay@eofficekukm.siwira.id', 'E-Office KUKM');
                        $message->to($user_reciver->email);
                    });
                }
            }

        } else {
            $logSuratInput['log_surat'] = 'upload_surat';
            $log_surat = $surat->log_surat()->create($logSuratInput);

            $suratTerkirimInput['sender_id'] = $request->user()->id;
            $suratTerkirimInput['log_surat_id'] = $log_surat->id;
            $suratTerkirimInput['progres_status'] = 'complete';
            $surat->surat_terkirim()->create($suratTerkirimInput);
        }

        return new SuratDetailResource($surat);
    }
}
