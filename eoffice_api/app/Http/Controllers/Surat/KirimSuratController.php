<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class KirimSuratController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $surat_id)
    {
        $surat = Surat::find($surat_id);
        if($surat) {
            $this->validate($request, [
                'keterangan' => 'required|string',
                'reciver_id' => 'required|array',
                'reciver_id.*' => 'required|numeric|exists:users,id',
            ]);
            $reciver_id = $request->input('reciver_id');

            $logSuratInput['user_id'] = $request->user()->id;
            $logSuratInput['log_surat'] = 'kirim_surat';
            $log_surat = $surat->log_surat()->create($logSuratInput);

            foreach($reciver_id as $key => $value) {
                $suratTerkirim = [
                    'sender_id' => $request->user()->id,
                    'reciver_id' => $value,
                    'keterangan' => $request->input('keterangan'),
                    'log_surat_id' => $log_surat->id,
                    'progres_status' => 'complete',
                ];
                $surat_terkirim = $surat->surat_terkirim()->create($suratTerkirim);
                $user_reciver = User::find($value);
                if(!empty($user_reciver->email)) {
                    $link = 'http://eofficekukm.siwira.id/surat-masuk/'.$surat->id.'/'.$surat_terkirim->id;
                    Mail::send('mail', ['data' => $surat, 'link' => $link], function ($message) use ($user_reciver) {
                        $message->subject('Notifikasi dari E-Office');
                        $message->from('noreplay@eofficekukm.siwira.id', 'E-Office KUKM');
                        $message->to($user_reciver->email);
                    });
                }
            }
            return new SuratDetailResource($surat);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
