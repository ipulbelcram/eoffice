<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;
use App\Models\SuratTerkirim;
use App\User;
use Illuminate\Http\Request;

class SuratTerkirimController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke($surat_terkirim_id)
    {
        $surat_terkirim = SuratTerkirim::find($surat_terkirim_id);
        if(!empty($surat_terkirim_id)) {
            return response()->json([
                'data' => [
                    'surat_id' => $surat_terkirim->surat_id,
                    'keterangan' => $surat_terkirim->keterangan,
                    'status_message' => $surat_terkirim->status_message,
                    'progres_status' => $surat_terkirim->progres_status
                ]
            ], 200);
        } else {
            return response()->json([
                'message' => 'data not found'
            ], 404);
        }
    }
}
