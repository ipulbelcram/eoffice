<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratKeluarResource;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class SuratKeluarController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $user_id = $request->user()->id;
        $suratKeluar = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', '!=', null], ['type', 'surat']])->groupBy('log_surat_id')->orderBy('created_at', 'DESC')->get();
        $totalSuratKeluar = count($suratKeluar);

        return response()->json([
            'total_surat_keluar' => $totalSuratKeluar,
            'data' => ListSuratKeluarResource::collection($suratKeluar),
        ], 200);
    }
}
