<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Models\Surat;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $surat_id)
    {
        $get_user_id = $request->user()->id;
        $surat = Surat::where([['id', $surat_id], ['user_id', $get_user_id]])->first();

        if(!empty($surat)) {
            $surat->delete();
            return response()->json([
                'message' => 'delete success'
            ]);
        } else {
            return response()->json([
                'message' => 'data not found'
            ], 404);
        }
    }
}
