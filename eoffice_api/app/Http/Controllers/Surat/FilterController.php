<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratKeluarResource;
use App\Models\VwSuratTerkirimDetail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class FilterController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function filterByJenisSurat(Request $request)
    {
        $this->validate($request, [
            'jenis_surat_id' => [
                'required',
                Rule::exists('params', 'id')->where(function($query) {
                    $query->where('category_param', 'jenis_surat');
                })
            ],
            'type_surat' => 'required|in:surat_masuk,surat_keluar,disposisi_masuk,disposisi_keluar',
        ]);
        $user_id = $request->user()->id;
        $jenis_surat_id = $request->input('jenis_surat_id');
        switch ($request->input('type_surat')) {
            case "surat_masuk": 
                $dataFilter = VwSuratTerkirimDetail::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', null], ['type', 'surat'], ['jenis_surat_id', $jenis_surat_id]])->orWhere([['reciver_id', $user_id], ['disposisi_id', null], ['type', 'surat'], ['jenis_surat_id', $jenis_surat_id]])->orderBy('created_at', 'DESC')->get();
                break;
            case "surat_keluar": 
                $dataFilter = VwSuratTerkirimDetail::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', '!=', null], ['type', 'surat'], ['jenis_surat_id', $jenis_surat_id]])->groupBy('log_surat_id')->orderBy('created_at', 'DESC')->get();
                break;
            case "disposisi_masuk": 
                $dataFilter = VwSuratTerkirimDetail::where([['reciver_id', $user_id], ['disposisi_id', '!=', null], ['type', 'surat'], ['jenis_surat_id', $jenis_surat_id]])->orderBy('created_at', 'DESC')->get();
                break;
            case "disposisi_keluar": 
                $dataFilter = VwSuratTerkirimDetail::where([['sender_id', $user_id], ['disposisi_id', '!=', null], ['type', 'surat'], ['jenis_surat_id', $jenis_surat_id]])->groupBy('log_surat_id')->orderBy('created_at', 'DESC')->get();
        }

        return response()->json([
            'total_surat' => count($dataFilter),
            'data' => ListSuratKeluarResource::collection($dataFilter),
        ], 200);
    }
}
