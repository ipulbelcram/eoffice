<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EditController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $surat_id)
    {
        $get_user_id = $request->user()->id;
        $surat = Surat::where([['id', $surat_id], ['user_id', $get_user_id]])->first();
        if(!empty($surat)) {
            // Validation 
            $this->validate($request, [
                'asal_surat' => 'required',
                'perihal' => 'required',
                'tanggal_surat' => 'required|date',
                'no_surat' => 'required|unique:surat,no_surat,'.$surat->id,
                'jenis_surat_id' => [
                    'required', 
                    'numeric', 
                    Rule::exists('params', 'id')->where(function ($query) {
                        $query->where('category_param', 'jenis_surat');
                    }),
                ],
                'file' => 'required|array',
                'file.*.file' => 'required|url',
                'file.*.file_name' => 'required|string',
            ]);
            
            // Create Variable
            $input = $request->all();
            $file = $request->input('file');

            // Edit Surat
            $surat->update($input);

            $surat->lampiran_surat()->delete();
            // Create File
            $files = [];
            foreach($file as $key => $value) {
                $fileInput = [
                    'file' => $value['file'],
                    'file_name' => $value['file_name'],
                ];
                $files[] = $fileInput;
            }
            $surat->lampiran_surat()->createMany($files);

            // Create Send Message and Log Message
            $logSuratInput['user_id'] = $get_user_id;
            $logSuratInput['log_surat'] = 'edit_surat';
            $log_surat = $surat->log_surat()->create($logSuratInput);
            return new SuratDetailResource($surat);
        } else {
            return response()->json([
                'message' => 'data not found'
            ], 404);
        }
    }
}
