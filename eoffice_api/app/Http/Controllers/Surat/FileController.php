<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Models\LampiranSurat;
use Illuminate\Http\Request;

class FileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function fileNameCek(Request $request)
    {
        $this->validate($request, [
            'file_name' => ['required', 'string']
        ]);

        $cek = LampiranSurat::where('file_name', $request->file_name)->count();
        return response()->json([
            'count' => $cek
        ], 200);
    }
}
