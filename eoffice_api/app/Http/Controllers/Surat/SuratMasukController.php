<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratResource;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class SuratMasukController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $id_user = null)
    {
        if($id_user != null) {
            $user_id = $id_user;
        } else {
            $user_id = $request->user()->id;
        }
        $suratMasuk = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', null], ['type', 'surat']])->orWhere([['reciver_id', $user_id], ['disposisi_id', null], ['type', 'surat']]);

        $totalSuratMasuk = $suratMasuk->count();
        $getSuratMasuk = $suratMasuk->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'total_surat_masuk' => $totalSuratMasuk,
            'data' => ListSuratResource::collection($getSuratMasuk),
        ], 200);
    }
}
