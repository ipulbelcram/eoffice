<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\Surat;

class DetailController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke($surat_id)
    {
        $surat = Surat::find($surat_id);

        if($surat) {
            return new SuratDetailResource($surat);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
