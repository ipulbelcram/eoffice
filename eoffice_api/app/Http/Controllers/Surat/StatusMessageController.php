<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class StatusMessageController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $surat_terkirim_id)
    {
        $reciver_id = $request->user()->id;
        $surat_terkirim = SuratTerkirim::where([['id', $surat_terkirim_id], ['reciver_id', $reciver_id]])->orWhere([['id', $surat_terkirim_id], ['reciver_id', NULL]])->first();
        if($surat_terkirim) {
            $data['status_message'] = 'read';
            $surat_terkirim->update($data);
            return response()->json(['status_message' => $data['status_message']], 200);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
