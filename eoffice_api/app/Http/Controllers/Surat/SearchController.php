<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratKeluarResource;
use App\Models\VwSuratTerkirimDetail;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function searchByNoAgenda(Request $request)
    {
        $this->validate($request, [
            'no_agenda' => 'required|numeric',
        ]);
        $no_agenda = $request->input('no_agenda');
        $user_id = $request->user()->id;
        $surat = VwSuratTerkirimDetail::where([['sender_id', $user_id], ['no_agenda', $no_agenda]])->orWhere([['reciver_id', $user_id], ['no_agenda', $no_agenda]])->groupBy('surat_id')->get();
        
        return ListSuratKeluarResource::collection($surat);
    }

    public function searchByNoSurat(Request $request)
    {
        $this->validate($request, [
            'no_surat' => 'required',
        ]);
        $no_surat = $request->input('no_surat');
        $user_id = $request->user()->id;
        $surat = VwSuratTerkirimDetail::where([['sender_id', $user_id], ['no_surat', 'like', '%'.$no_surat.'%'], ['no_agenda', '!=', null]])->orWhere([['reciver_id', $user_id], ['no_surat', 'like', '%'.$no_surat.'%'], ['no_agenda', '!=', null]])->groupBy('surat_id')->orderBy('created_at','DESC')->get();
        
        return ListSuratKeluarResource::collection($surat);
    }
}
