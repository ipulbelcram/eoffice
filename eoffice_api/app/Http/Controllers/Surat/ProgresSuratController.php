<?php

namespace App\Http\Controllers\Surat;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\LogSuratResource;
use Illuminate\Http\Request;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Models\LogSurat;
use App\Models\Surat;
use App\Models\SuratTerkirim;

class ProgresSuratController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function progres(Request $request, $surat_terkirim_id)
    {
        $surat_terkirim = SuratTerkirim::find($surat_terkirim_id);
        $surat_terkirim->update([
            'progres_status' => 'progres' 
        ]);

        $input['user_id'] = $request->user()->id;
        $input['surat_id'] = $surat_terkirim->surat_id;
        $input['log_surat'] = 'proses_disposisi';
        $logSurat = LogSurat::create($input);
        return new LogSuratResource($logSurat);
    }

    public function finish(Request $request, $surat_terkirim_id)
    {
        $surat_terkirim = SuratTerkirim::find($surat_terkirim_id);
        $surat_terkirim->update([
            'progres_status' => 'finish' 
        ]);

        $input['user_id'] = $request->user()->id;
        $input['surat_id'] = $surat_terkirim->surat_id;
        $input['log_surat'] = 'finish_disposisi';
        $logSurat = LogSurat::create($input);
        return new LogSuratResource($logSurat);
    }
}
