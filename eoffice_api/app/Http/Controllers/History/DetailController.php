<?php

namespace App\Http\Controllers\History;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReciverResource;
use App\Http\Resources\Surat\SuratDetailResource;
use App\Http\Resources\UserResource;
use App\Models\LogSurat;
use App\Models\Surat;
use App\Models\SuratTerkirim;
use App\User;

class DetailController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke($log_surat_id)
    {
        $log_surat = LogSurat::find($log_surat_id);

        if($log_surat) {
            $surat = Surat::find($log_surat->surat_id);
            $sender = User::find($log_surat->user_id);
            $suratTerkirim = SuratTerkirim::where('log_surat_id', $log_surat->id)->get();

            foreach($suratTerkirim as $suratTer) {
                $userTer = User::find($suratTer->reciver_id);
                $reciver = [
                    'id' => $userTer->id,
                    'username' => $userTer->username,
                    'name' => $userTer->name,
                    'foto' => $userTer->foto,
                    'status_message' => $suratTer->status_message,
                    'keterangan' => $suratTer->keterangan,
                ];
                $recivers[] = $reciver;
            }
            return response()->json([
                'data' => [
                    'surat' => new SuratDetailResource($surat),
                    'sender' => new UserResource($sender),
                    'reciver' => $recivers,
                ]
            ], 200);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
