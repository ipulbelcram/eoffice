<?php

namespace App\Http\Controllers\Param;

use App\Http\Controllers\Controller;
use App\Http\Resources\Disposisi\InstruksiResource;
use App\Http\Resources\Params\JabatanResource;
use App\Http\Resources\Params\JenisSuratResource;
use App\Http\Resources\Params\SifatDisposisiResource;
use App\Http\Resources\Params\UserLevelResource;
use App\Models\Param;
use App\Models\UserLevel;

class GetParamController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getJenisSurat()
    {
        $jenis_surat = Param::where([['category_param', 'jenis_surat'], ['active', 1]])->orderBy('order', 'ASC')->get();
        return JenisSuratResource::collection($jenis_surat);
    }

    public function getSifatDisposisi()
    {
        $sifat_disposisi = Param::where([['category_param', 'sifat_disposisi'], ['active', 1]])->orderBy('order', 'ASC')->get();
        return SifatDisposisiResource::collection($sifat_disposisi);
    }

    public function getInstruksi()
    {
        $instruksi = Param::where([['category_param', 'instruksi'], ['active', 1]])->orderBy('order', 'ASC')->get();
        return InstruksiResource::collection($instruksi);
    }

    public function getJabatan()
    {
        $jabatan = Param::where([['category_param', 'jabatan'], ['active', 1]])->get();
        return JabatanResource::collection($jabatan);
    }

    public function getUserLevel()
    {
        $user_level = UserLevel::all();
        return UserLevelResource::collection($user_level);
    }
}
