<?php

namespace App\Http\Controllers\Disposisi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Disposisi\DisposisiDetailResource;
use App\Models\Disposisi;

class DetailController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke($disposisi_id)
    {
        $disposisi = Disposisi::find($disposisi_id);

        if($disposisi) {
            return new DisposisiDetailResource($disposisi);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
