<?php

namespace App\Http\Controllers\Disposisi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratResource;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class DisposisiMasukController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $user_id = $request->user()->id;
        $disposisiMasuk = SuratTerkirim::where([['reciver_id', $user_id], ['disposisi_id', '!=', null]]);

        $totalDisposisiMasuk = $disposisiMasuk->count();
        $getDisposisiMasuk = $disposisiMasuk->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'total_disposisi_masuk' => $totalDisposisiMasuk,
            'data' => ListSuratResource::collection($getDisposisiMasuk),
        ]);
    }
}
