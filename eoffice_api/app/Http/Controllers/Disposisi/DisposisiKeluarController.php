<?php

namespace App\Http\Controllers\Disposisi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Surat\ListSuratKeluarResource;
use App\Models\SuratTerkirim;
use Illuminate\Http\Request;

class DisposisiKeluarController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request)
    {
        $user_id = $request->user()->id;
        $disposisiKeluar = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', '!=', null]])->groupBy('log_surat_id')->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'total_disposisi_keluar' => count($disposisiKeluar),
            'data' => ListSuratKeluarResource::collection($disposisiKeluar),
        ]);
    }
}
