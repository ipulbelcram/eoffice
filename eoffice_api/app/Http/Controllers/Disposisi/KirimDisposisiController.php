<?php

namespace App\Http\Controllers\Disposisi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Disposisi\DisposisiDetailResource;
use App\Models\Surat;
use App\Models\SuratTerkirim;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class KirimDisposisiController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $surat_id, $surat_terkirim_id)
    {
        $surat = Surat::find($surat_id);
        $last_surat_terkirim = SuratTerkirim::findOrFail($surat_terkirim_id);
        if($surat) {
            $this->validate($request, [
                'sifat_id' => [
                    'required', 
                    'numeric', 
                    Rule::exists('params', 'id')->where(function($query) {
                        $query->where('category_param', 'sifat_disposisi');
                    }),
                ],
                'keterangan' => 'required|string',
                'instruksi_id' => 'required|array',
                'instruksi_id.*' => [
                    'required', 
                    'numeric', 
                    Rule::exists('params', 'id')->where(function($query) {
                        $query->where('category_param', 'instruksi');
                    }),
                ],
                'reciver_id' => 'required|array',
                'reciver_id.*' => 'required|numeric|exists:users,id',
            ]);
            $input = $request->all();
            $disposisi = $surat->disposisi()->create($input);
            $instruksi_id = $request->input('instruksi_id');

            $instruksiInput = [];
            foreach($instruksi_id as $key => $value) {
                $instruksi = [
                    'instruksi_id' => $value
                ];
                $instruksiInput[] = $instruksi;
            }
            $disposisi->instruksi()->attach($instruksiInput);

            $reciver_id = $request->input('reciver_id');

            $logSuratInput['user_id'] = $request->user()->id;
            $logSuratInput['log_surat'] = 'kirim_disposisi';
            $log_surat = $surat->log_surat()->create($logSuratInput);

            foreach($reciver_id  as $key => $value) {
                $reciver = [
                    'surat_id' => $surat_id,
                    'sender_id' => $request->user()->id,
                    'reciver_id' => $value,
                    'keterangan' => $request->input('keterangan'),
                    'log_surat_id' => $log_surat->id,
                ];
                $surat_terkirim = $disposisi->surat_terkirim()->create($reciver);
                $user_reciver = User::find($value);
                if(!empty($user_reciver->email)) {
                    $link = 'http://eofficekukm.siwira.id/disposisi-masuk/'.$surat->id.'/'.$surat_terkirim->id;
                    Mail::send('mail', ['data' => $surat, 'link' => $link], function ($message) use ($user_reciver) {
                        $message->subject('Notifikasi dari E-Office');
                        $message->from('noreplay@eofficekukm.siwira.id', 'E-Office KUKM');
                        $message->to($user_reciver->email);
                    });
                }
            }

            if(!empty($last_surat_terkirim)) {
                $updateSuratTerkirim['progres_status'] = 'kirim_disposisi';
                $last_surat_terkirim->update($updateSuratTerkirim);
            }
            return new DisposisiDetailResource($disposisi);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
