<?php

namespace App\Http\Controllers\Disposisi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Disposisi\DisposisiDetailResource;
use App\Models\Disposisi;
use App\Models\Surat;
use App\Models\SuratTerkirim;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TeruskanDisposisiController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function __invoke(Request $request, $disposisi_id, $surat_terkirim_id)
    {
        $disposisi = Disposisi::find($disposisi_id);
        $surat = Surat::find($disposisi->surat_id);
        $last_surat_terkirim = SuratTerkirim::findOrFail($surat_terkirim_id);
        if($disposisi) {
            $this->validate($request, [
                'reciver_id' => 'required|array',
                'reciver_id.*' => 'required|exists:users,id',
                'keterangan' => 'required|string',
            ]);

            $reciver_id = $request->input('reciver_id');

            $logSuratInput['user_id'] = $request->user()->id;
            $logSuratInput['log_surat'] = 'kirim_disposisi';
            $log_surat = $surat->log_surat()->create($logSuratInput);

            foreach($reciver_id as $key => $value) {
                $reciver = [
                    'surat_id' => $disposisi->surat_id,
                    'sender_id' => $request->user()->id,
                    'reciver_id' => $value,
                    'keterangan' => $request->input('keterangan'),
                    'log_surat_id' => $log_surat->id,
                ];
                $surat_terkirim = $disposisi->surat_terkirim()->create($reciver);
                $user_reciver = User::find($value);
                if(!empty($user_reciver->email)) {
                    $link = 'http://eofficekukm.siwira.id/disposisi-masuk/'.$surat->id.'/'.$surat_terkirim->id;
                    Mail::send('mail', ['data' => $surat, 'link' => $link], function ($message) use ($user_reciver) {
                        $message->subject('Notifikasi dari E-Office');
                        $message->from('noreplay@eofficekukm.siwira.id', 'E-Office KUKM');
                        $message->to($user_reciver->email);
                    });
                }
            }

            if(!empty($last_surat_terkirim)) {
                $updateSuratTerkirim['progres_status'] = 'teruskan_disposisi';
                $last_surat_terkirim->update($updateSuratTerkirim);
            }
            return new DisposisiDetailResource($disposisi);
        } else {
            return response()->json(['data not found'], 404);
        }
    }
}
