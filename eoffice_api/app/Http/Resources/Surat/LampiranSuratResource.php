<?php

namespace App\Http\Resources\Surat;

use Illuminate\Http\Resources\Json\JsonResource;

class LampiranSuratResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'file' => $this->file,
           'file_name' => $this->file_name,
        ];
    }
}