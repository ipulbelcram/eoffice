<?php

namespace App\Http\Resources\Surat;

use App\Http\Resources\Disposisi\DisposisiResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListSuratResource extends JsonResource
{
    public function toArray($request)
    {
        if($this->disposisi_id == null ) {
            return [
                'surat_terkirim_id' => $this->id,
                'surat' => new SuratResource($this->surat),
                'sender' => new UserResource($this->sender),
                'reciver' => new UserResource($this->reciver),
                'keterangan' => $this->keterangan,
                'status_message' => $this->status_message,
                'progres_status' => $this->progres_status,
            ];
        } else {
            return [
                'surat_terkirim_id' => $this->id,
                'disposisi' =>  new DisposisiResource($this->disposisi),
                'sender' => new UserResource($this->sender),
                'reciver' => new UserResource($this->reciver),
                'surat' => new SuratResource($this->surat),
                'keterangan' => $this->keterangan,
                'status_message' => $this->status_message,
                'progres_status' => $this->progres_status,
             ];
        }

    }
}