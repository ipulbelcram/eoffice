<?php

namespace App\Http\Resources\Surat;

use App\Http\Resources\Params\JenisSuratResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SuratResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'no_agenda' => $this->no_agenda,
           'no_surat' => $this->no_surat,
           'asal_surat' => $this->asal_surat,
           'perihal' => $this->perihal,
           'jenis_surat' => new JenisSuratResource($this->jenis_surat),
           'tanggal_surat' => $this->tanggal_surat,
           'created_at' => \Carbon\Carbon::parse($this->created_at)->format('d-M-Y h:i:s'),
        ];
    }
}