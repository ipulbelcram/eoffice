<?php

namespace App\Http\Resources\Surat;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LogSuratResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'user' => new UserResource($this->user),
           'log_surat' => $this->log_surat,
           'created_at' => \Carbon\Carbon::parse($this->created_at)->format('d-M-Y h:i:s'),
        ];
    }
}