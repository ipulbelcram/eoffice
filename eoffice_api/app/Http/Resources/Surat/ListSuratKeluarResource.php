<?php

namespace App\Http\Resources\Surat;

use App\Http\Resources\Disposisi\DisposisiResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListSuratKeluarResource extends JsonResource
{
    public function toArray($request)
    {
        if($this->disposisi_id == null ) {
            return [
                'surat_terkirim_id' => $this->id,
                'surat' => new SuratResource($this->surat),
                'sender' => new UserResource($this->sender),
                'progres_status' => $this->progres_status,
            ];
        } else {
            return [
                'surat_terkirim_id' => $this->id,
                'disposisi' =>  new DisposisiResource($this->disposisi),
                'sender' => new UserResource($this->sender),
                'surat' => new SuratResource($this->surat),
                'progres_status' => $this->progres_status,
             ];
        }

    }
}