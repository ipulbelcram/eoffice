<?php

namespace App\Http\Resources\Surat;

use App\Http\Resources\Params\JenisSuratResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\Surat\LampiranSuratResource;
use App\Http\Resources\Surat\LogSuratResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SuratDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'user' => new UserResource($this->user),
           'no_agenda' => $this->no_agenda,
           'asal_surat' => $this->asal_surat,
           'perihal' => $this->perihal,
           'tanggal_surat' => $this->tanggal_surat,
           'no_surat' => $this->no_surat,
           'jenis_surat' => new JenisSuratResource($this->jenis_surat),
           'file' => LampiranSuratResource::collection($this->lampiran_surat),
           'log_surat' => LogSuratResource::collection($this->log_surat),
           'created_at' => \Carbon\Carbon::parse($this->created_at)->format('d-M-Y h:i:s'),
        ];
    }
}