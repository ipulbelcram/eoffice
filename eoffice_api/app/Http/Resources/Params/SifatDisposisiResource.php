<?php

namespace App\Http\Resources\Params;

use Illuminate\Http\Resources\Json\JsonResource;

class SifatDisposisiResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'sifat_diposisi' => $this->param
        ];
    }
}