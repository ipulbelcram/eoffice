<?php

namespace App\Http\Resources\Params;

use Illuminate\Http\Resources\Json\JsonResource;

class JenisSuratResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'jenis_surat' => $this->param
        ];
    }
}