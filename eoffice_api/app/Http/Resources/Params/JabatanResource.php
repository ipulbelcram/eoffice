<?php

namespace App\Http\Resources\Params;

use Illuminate\Http\Resources\Json\JsonResource;

class JabatanResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'jabatan' => $this->param
        ];
    }
}