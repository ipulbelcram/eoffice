<?php

namespace App\Http\Resources\Params;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLevelResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'user_type' => $this->user_type
        ];
    }
}