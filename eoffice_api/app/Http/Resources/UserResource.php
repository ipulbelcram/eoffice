<?php

namespace App\Http\Resources;

use App\Models\SuratTerkirim;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        $user_id = $this->id;
        $suratMasuk = SuratTerkirim::where([['sender_id', $user_id], ['disposisi_id', null], ['reciver_id', null], ['type', 'surat']])->orWhere([['reciver_id', $user_id], ['disposisi_id', null], ['type', 'surat']]);
        return [
            'id' => $this->id,
            'name' => $this->name,
            // 'jabatan' => new JabatanResource($this->jabatan),
            'jabatan' => $this->jabatan_name,
            'foto' => $this->foto,
            'total_surat_masuk' => $suratMasuk->count() 
        ];
    }
}
