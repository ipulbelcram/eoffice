<?php

namespace App\Http\Resources\Disposisi;

use Illuminate\Http\Resources\Json\JsonResource;

class InstruksiResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'instruksi_id' => $this->id,
            'instruksi' => $this->param,
        ];
    }
}