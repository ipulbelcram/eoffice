<?php

namespace App\Http\Resources\Disposisi;

use App\Http\Resources\Params\SifatDisposisiResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DisposisiResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sifat' => new SifatDisposisiResource($this->sifat),
            'created_at' => \Carbon\Carbon::parse($this->created_at)->format('d-M-Y h:i:s'),
        ];
    }
}