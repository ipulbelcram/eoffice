<?php

namespace App\Http\Resources\Disposisi;

use App\Http\Resources\Params\SifatDisposisiResource;
use App\Http\Resources\Surat\SuratDetailResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DisposisiDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'surat' => new SuratDetailResource($this->surat),
            'sifat' => new SifatDisposisiResource($this->sifat),
            'instruksi' =>  InstruksiResource::collection($this->instruksi),
            'created_at' => \Carbon\Carbon::parse($this->created_at)->format('d-M-Y h:i:s'),
        ];
    }
}