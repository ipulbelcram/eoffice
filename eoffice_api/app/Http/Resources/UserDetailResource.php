<?php

namespace App\Http\Resources;

use App\Http\Resources\Params\JabatanResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'email' => $this->email,
            'nip' => $this->nip,
            'jenis_kelamin' => $this->jenis_kelamin,
            'no_hp' => $this->no_hp,
            'jabatan' => (!empty($this->jabatan) ? new JabatanResource($this->jabatan) : null), 
            'jabatan_name' => $this->jabatan_name,
            'user_level_id' => $this->user_level_id,
            'foto' => $this->foto,
        ];
    }
}