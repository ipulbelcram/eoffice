<?php

Route::get('session/login','SessionController@createSession');
Route::get('session/logout','SessionController@deleteSession');
Route::get('/clear-cache', function() {
	Artisan::call('cache:clear');
	Artisan::call('view:clear');
	return redirect('/');
});

Route::group(['middleware'=>['afterMiddleware']], function () {
	Route::get('/', function () {
		return view('auth/login');
	});
});

Route::group(['middleware'=>['beforeMiddleware']], function () {
	Route::get('dashboard', function () {
		return view('dashboard');
	});
	Route::get('profil', function () {
		return view('profil');
	});
	Route::get('ubah-password', function () {
		return view('ubah-password');
	});

	Route::get('user', function () {
		return view('user');
	});
	Route::get('user/{id}', function () {
		return view('edit-user');
	});
	Route::get('create/user', function () {
		return view('create-user');
	});
	Route::get('user/{id}/surat-masuk', function () {
		return view('user-surat-masuk');
	});
	Route::get('user/surat-masuk/{id}', function () {
		return view('user-detail-surat-masuk');
	});

	Route::get('surat-masuk', function () {
		return view('surat-masuk');
	});
	Route::get('surat-masuk/{id}/{id_terkirim}', function ($id, $id_terkirim) {
		return view('detail-surat-masuk');
	});
	Route::get('surat-keluar', function () {
		return view('surat-keluar');
	});
	Route::get('surat-keluar/{id}/{id_terkirim}', function ($id, $id_terkirim) {
		return view('detail-surat-keluar');
	});

	Route::get('disposisi-masuk', function () {
		return view('disposisi-masuk');
	});
	Route::get('disposisi-masuk/{id}/{id_terkirim}', function ($id, $id_terkirim) {
		return view('detail-disposisi-masuk');
	});
	Route::get('disposisi-keluar', function () {
		return view('disposisi-keluar');
	});
	Route::get('disposisi-keluar/{id}', function ($id) {
		return view('detail-disposisi-keluar');
	});

	Route::get('informasi-masuk', function () {
		return view('informasi-masuk');
	});
	Route::get('informasi-masuk/{id}/{id_terkirim}', function ($id, $id_terkirim) {
		return view('detail-informasi-masuk');
	});
	Route::get('informasi-keluar', function () {
		return view('informasi-keluar');
	});
	Route::get('informasi-keluar/{id}', function ($id) {
		return view('detail-informasi-keluar');
	});

	Route::get('tambah/surat-masuk', function () {
		return view('tambah-surat-masuk');
	});
	Route::get('tambah/surat-keluar', function () {
		return view('tambah-surat-keluar');
	});
	Route::get('tambah/informasi', function () {
		return view('tambah-informasi');
	});
	Route::get('edit-surat/{id}/{id_terkirim}', function ($id, $id_terkirim) {
		return view('edit-surat');
	});
	
	Route::get('history/{id}', function ($id) {
		return view('history');
	});

	Route::get('tracking-surat', function () {
		return view('tracking-surat');
	});
});