@extends('layouts/app')

@section('title','Ubah Password')

@section('style')
	<style type="text/css">
		.password {
			position: absolute;
			right: 15px;
			top: 0px;
			cursor: pointer;
			color: rgb(0,0,0,0.5);
			padding: 8px 10px;
		}
		.password.invalid {
			right: 40px;
		}
	</style>
@endsection

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Ubah Password</p>
		<form id="form">
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Password Lama</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="password" maxlength="32" autocomplete="autocomplete" autofocus="autofocus">
					<div class="invalid-feedback" id="password-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="password"></i>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Password Baru</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="npassword" minlength="8" maxlength="32" autocomplete="autocomplete">
					<div class="invalid-feedback" id="npassword-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="npassword"></i>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Konfirmasi Password</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="cpassword" minlength="8" maxlength="32" autocomplete="autocomplete">
					<div class="invalid-feedback" id="cpassword-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="cpassword"></i>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit">
						<span id="load" class="none"><i class="mdi mdi-spin mdi-loading pr-0"></i></span>
						<span id="text">Ubah Password</span>
					</button>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/api/ubah-password.js')}}"></script>
@endsection