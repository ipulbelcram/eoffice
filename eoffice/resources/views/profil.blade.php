@extends('layouts/app')

@section('title','Profil')

@section('style')
	<link rel="stylesheet" href="{{asset('assets/vendors/croppie/croppie.css')}}">
@endsection

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Profil</p>
		<div class="form-row align-items-center mt-4 pb-3">
			<div class="col-xl-3 col-lg-4 col-sm-5 d-none d-sm-block">
				<label>Foto Profil</label>
			</div>
			<div class="col-xl-5 col-lg-6 col-sm-7 text-center text-sm-left">
				<div class="d-inline-block" data-toggle="modal" data-target="#modalAvatar" role="button">
					<img src="{{asset('assets/images/photo.png')}}" class="avatar rounded-circle" width="100"><br>
					<div class="btn btn-link btn-sm ml-sm-2" data-toggle="modal" data-target="#modalAvatar">Ubah Foto</div>
				</div>
			</div>
		</div>
		<form id="form">
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nama Lengkap</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="name">
				</div>
			</div>
			<div class="form-group row">
				<label for="email" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Email</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input type="email" class="form-control" id="email">
					<div class="invalid-feedback" id="email-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="jabatan" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Jabatan</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input type="email" class="form-control" id="jabatan" disabled="disabled">
				</div>
			</div>

			<hr class="mt-4">
			<p class="text-secondary text-uppercase">Informasi Pribadi</p>
			<div class="form-group row">
				<label for="username" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Username</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="username" disabled="disabled">
				</div>
			</div>
			<div class="form-group row">
				<label for="nip" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">NIP</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="nip" minlength="18" maxlength="18">
					<div class="invalid-feedback" id="nip-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="gender" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Jenis Kelamin</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 pt-1">
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="gender" id="male" value="laki-laki">
						<label class="form-check-label" for="male" role="button">Laki-Laki</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="gender" id="female" value="perempuan">
						<label class="form-check-label" for="female" role="button">Perempuan</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="phone" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nomor Telepon</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text" id="plus">
								<small>+62</small>
							</div>
						</div>
						<input type="tel" class="form-control" id="phone" aria-describedby="plus">
					</div>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit">
						<span id="load" class="none"><i class="mdi mdi-spin mdi-loading pr-0"></i></span>
						<span id="text">Simpan Profil</span>
					</button>
				</div>
			</div>
		</form>
	</div>

    <div class="modal fade" id="modalAvatar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title">Ubah Foto Profil</h5>
					<i class="mdi mdi-close mdi-18px float-right pr-0" role="button" data-dismiss="modal"></i>
				</div>
                <div class="modal-body pt-0">
                    <div class="text-center" id="avatar-form">
                    	<input type="file" class="d-none" id="avatar" accept="image/jpeg">
                    	<label for="avatar" role="button">
                    		<i class="d-block mdi mdi-camera mdi-48px bg-light rounded-circle border py-1 px-3"></i>Upload Foto
                    	</label>
                    </div>
                    <div class="pt-4" id="avatar-preview" style="display:none"></div>
                    <p class="text-center text-danger none" id="feedback-file"></p>
                </div>
                <div class="modal-footer none">
                    <div class="text-right">
                        <button class="btn btn-sm btn-link px-4" id="back">Batal</button>
                        <button class="btn btn-sm btn-primary px-4" id="upload" disabled="disabled">
							<span id="loadAvatar" class="none"><i class="mdi mdi-spin mdi-loading"></i></span>
							<span id="text">Simpan</span>
		        		</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
	<script type="text/javascript">
		const id = '{{session("id")}}'
		const email = '{{session("email")}}'
		const username = '{{session("username")}}'
		const nip = '{{session("nip")}}'
		const gender = '{{session("gender")}}'
		const phone = '{{session("phone")}}'
	</script>
    <script type="text/javascript" src="{{asset('assets/vendors/croppie/croppie.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/profil.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/avatar.js')}}"></script>
@endsection