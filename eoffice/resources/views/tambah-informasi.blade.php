@extends('layouts/app')

@section('title','Tambah Informasi')

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Tambah Informasi</p>
		<form id="form" enctype="multipart/form-data">
			<div class="form-group row">
				<label for="no_surat" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Nomor Informasi</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<input class="form-control" id="no_surat" autofocus="autofocus">
					<div class="invalid-feedback" id="no_surat-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="asal_surat" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Asal Informasi</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<input class="form-control" id="asal_surat">
					<div class="invalid-feedback" id="asal_surat-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="tanggal_surat" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Tanggal Informasi</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<input type="date" class="form-control" id="tanggal_surat">
					<div class="invalid-feedback" id="tanggal_surat-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="penerima" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Penerima</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<div class="form-control reciver" data-toggle="modal" data-target="#modal-reciver" role="button">
						<span class="text-secondary" id="total-reciver" style="font-size:14px">Pilih Penerima</span>
					</div>
					<div class="invalid-feedback" id="reciver-feedback"></div>
				</div>
			</div>
			<div class="form-group row mb-0">
				<label for="lampiran" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Lampiran</label>
				<div class="col-xl-5 col-lg-6 col-md-7" id="form-file">
					<div class="file-group mb-3">
						<div class="custom-file" data-index="0">
							<input type="file" class="file custom-file-input" role="button">
							<label class="custom-file-label">Pilih Lampiran</label>
							<div class="invalid-feedback"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<div class="small text-danger pt-1" id="file-feedback"></div>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit">
						<div class="loader none" id="load">
							<svg class="circular" viewBox="25 25 50 50">
								<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
							</svg>
						</div>
						<span id="text">Kirim Informasi</span>
					</button>
				</div>
			</div>
		</form>
	</div>
	<div class="modal fade" id="modal-reciver" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h6 class="modal-title text-capitalize">Pilih Penerima</h6>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body py-0">
					<div class="form-group">
						<input class="form-control" id="search-reciver" placeholder="Cari...">
					</div>
					<div id="reciver" class="text-truncate overflow-auto" style="height:235px">
						<div id="empty" class="text-center text-secondary none">Data tidak ditemukan.</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-link px-4" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-sm btn-primary px-4" id="btn-select" data-dismiss="modal">Pilih Penerima</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/api/add-file.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/add-reciver.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/tambah-informasi.js')}}"></script>
@endsection