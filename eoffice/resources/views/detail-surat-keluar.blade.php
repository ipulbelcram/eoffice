@extends('layouts/app')

@section('content')
	<div class="container">
		<div class="d-flex align-items-center mb-3">
			<a href="{{url('surat-keluar')}}" class="text-dark"><i class="mdi mdi-arrow-left mdi-18px"></i></a>
			<div class="none ml-auto" id="action">
				<a class="text-dark mr-4" id="edit"><i class="mdi mdi-18px mdi-pencil-outline pr-0"></i></a>
				<i class="mdi mdi-18px mdi-trash-can-outline pr-0" role="button" data-toggle="modal" data-target="#modal-delete"></i>
			</div>
		</div>
		<div class="clearfix">
			<img src="{{asset('assets/images/photo.png')}}" width="40" class="rounded-circle float-left mr-3" id="foto">
			<div class="d-inline-flex flex-column">
				<span id="sender"></span>
				<small role="button">Detail <i class="mdi mdi-chevron-up"></i></small>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6 col-lg-7 col-md-8">
				<div class="card rounded mt-3">
					<div class="card-body py-3">
						<table>
							<tbody>
								<tr>
									<td>Nomor Agenda</td>
									<td>:</td>
									<td id="no_agenda"></td>
								</tr>
								<tr>
									<td>Nomor Surat</td>
									<td>:</td>
									<td id="no_surat"></td>
								</tr>
								<tr>
									<td>Asal Surat</td>
									<td>:</td>
									<td id="asal_surat"></td>
								</tr>
								<tr>
									<td>Tanggal Surat</td>
									<td>:</td>
									<td id="tanggal_surat"></td>
								</tr>
								<tr>
									<td>Jenis Surat</td>
									<td>:</td>
									<td id="jenis_surat"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="mt-3">
			<span class="text-secondary">Perihal :</span>
			<p id="perihal"></p>
		</div>
		<hr>
		<div class="mb-2">
			<i class="mdi mdi-attachment mdi-rotate-135 pr-0"></i> <span id="total-file"></span> Lampiran
		</div>
		<div class="row" id="file"></div>
		<hr>
		<div class="mt-3">
			<span>History Surat</span>
			<div class="mt-2" id="log-surat"></div>
		</div>
	</div>

	<div class="modal fade" id="modal-delete" tabindex="-1" aria-hidden="true">
		<div class="modal-sm modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title">Hapus Surat?</h5>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body py-0">
					<span>Semua surat yang telah <b>diteruskan</b> & <b>didisposisikan</b> akan ikut terhapus.</span>
				</div>
				<div class="modal-footer border-top-0">
					<div class="btn btn-sm btn-link" data-dismiss="modal">Batal</div>
					<button class="btn btn-sm btn-link" id="delete">Hapus</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">const id = '{{Request::route("id")}}'</script>
	<script type="text/javascript">const id_terkirim = '{{Request::route("id_terkirim")}}'</script>
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/detail-surat-keluar.js')}}"></script>
@endsection