@extends('layouts/app')

@section('title','Tambah User')

@section('style')
	<style>
		.password {
			position: absolute;
			right: 15px;
			top: 0px;
			cursor: pointer;
			color: rgb(0,0,0,0.5);
			padding: 8px 10px;
		}
		.password.invalid {
			right: 40px;
		}
	</style>
@endsection

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Tambah User</p>
		<form id="form" class="hide">
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nama Lengkap</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="name" autofocus="autofocus">
					<div class="invalid-feedback" id="name-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="email" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Email</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input type="email" class="form-control" id="email">
					<div class="invalid-feedback" id="email-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="nip" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">NIP</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="nip" minlength="18" maxlength="18">
					<div class="invalid-feedback" id="nip-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="gender" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Jenis Kelamin</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 pt-1">
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="gender" id="male" value="laki-laki">
						<label class="form-check-label" for="male" role="button">Laki-Laki</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="gender" id="female" value="perempuan">
						<label class="form-check-label" for="female" role="button">Perempuan</label>
					</div>
					<div class="invalid-feedback" id="gender-feedback">asd</div>
				</div>
			</div>
			<!-- <div class="form-group row">
				<label for="phone" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nomor Telepon</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text" id="plus">
								<small>+62</small>
							</div>
						</div>
						<input type="tel" class="form-control" id="phone" aria-describedby="plus">
					</div>
				</div>
			</div> -->

			<div class="form-group row mt-5">
				<label for="jabatan_id" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Jabatan</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<select class="custom-select" id="jabatan_id"></select>
					<div class="invalid-feedback" id="jabatan_id-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="jabatan_name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nama Jabatan</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="jabatan_name">
					<div class="invalid-feedback" id="jabatan_name-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="atasan_id" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Atasan</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<select class="custom-select" id="atasan_id" disabled>
						<option disabled selected>Pilih</option>
					</select>
					<div class="invalid-feedback" id="atasan_id-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="asdep_id" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Asdep</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<select class="custom-select" id="asdep_id" disabled>
						<option disabled selected>Pilih</option>
					</select>
					<div class="invalid-feedback" id="asdep_id-feedback"></div>
				</div>
			</div>

			<div class="form-group row mt-5">
				<label for="username" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Username</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="username">
					<div class="invalid-feedback" id="username-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Password Baru</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="npassword" minlength="8" maxlength="32" autocomplete="autocomplete">
					<div class="invalid-feedback" id="npassword-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="npassword"></i>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Konfirmasi Password</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="cpassword" minlength="8" maxlength="32" autocomplete="autocomplete">
					<div class="invalid-feedback" id="cpassword-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="cpassword"></i>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit">
						<span id="load" class="none"><i class="mdi mdi-spin mdi-loading pr-0"></i></span>
						<span id="text">Simpan</span>
					</button>
				</div>
			</div>
		</form>
		<div class="d-flex flex-column justify-content-center align-items-center state" id="loading">
			<div class="loader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="pathp" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
				</svg>
			</div>
		</div>
    </div>
@endsection

@section('script')
	<script src="{{asset('assets/api/create-user.js')}}"></script>
@endsection