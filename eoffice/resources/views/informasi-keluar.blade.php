@extends('layouts/app')

@section('title','Informasi Keluar')

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase none-i" id="head">Informasi Keluar</p>
		<!-- <hr class="mx-3 my-1 d-none d-md-block"> -->
		<div class="box-container mb-5" id="data"></div>
		<div class="d-flex flex-column justify-content-center align-items-center none-i" id="empty" style="height:60vh;">
			<img src="{{asset('assets/images/empty.svg')}}" width="150">
			<h6 class="font-weight-normal text-secondary mt-4">Informasi Keluar kosong</h6>
		</div>
		<div class="compose">
			<a href="{{url('tambah/informasi')}}" class="btn btn-sm btn-primary d-flex align-items-center px-3" style="border-radius:100px">
				<i class="mdi mdi-pencil-outline mdi-18px"></i> Tambah Informasi
			</a>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/informasi-keluar.js')}}"></script>
@endsection