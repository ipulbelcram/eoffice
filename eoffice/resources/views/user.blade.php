@extends('layouts/app')

@section('title','User')

@section('style')
	<style>
		#table td {
		    vertical-align: middle !important;
		}
	</style>
@endsection

@section('content')
	<div class="container">
		<div class="mb-5 none-i" id="data">
			<h5 class="mb-3">User</h5>
			<div class="table-responsive">
				<table class="table border align-items-center">
					<thead>
						<tr>
							<th><i class="mdi mdi-check-all mdi-checkbox-blank-outline mdi-18px pr-0" role="button"></i></th>
							<th class="text-truncate">Nama Lengkap</th>
							<th class="text-truncate">Jabatan</th>
							<th class="text-truncate">Foto</th>
							<th class="text-truncate">Total Surat Masuk</th>
						</tr>
					</thead>
					<tbody id="table"></tbody>
					<tbody id="loading_table" class="none">
						<tr>
							<td colspan="9" class="text-center">
								<img src="{{asset('assets/images/loader.gif')}}" width="25">
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<nav id="pagination">
				<ul class="pagination pb-3" data-filter="request">
					<li class="page page-item disabled" id="first" role="button">
						<span class="page-link"><i class="pr-0 mdi mdi-chevron-double-left"></i></span>
					</li>
					<li class="page page-item disabled" id="prev" role="button">
						<span class="page-link"><i class="pr-0 mdi mdi-chevron-left"></i></span>
					</li>
					<li class="page page-item" id="prevCurrentDouble" role="button"><span class="page-link"></span></li>
					<li class="page page-item" id="prevCurrent" role="button"><span class="page-link"></span></li>
					<li class="page page-item" id="current" role="button"><span class="page-link"></span></li>
					<li class="page page-item" id="nextCurrent" role="button"><span class="page-link"></span></li>
					<li class="page page-item" id="nextCurrentDouble" role="button"><span class="page-link"></span></li>
					<li class="page page-item" id="next" role="button">
						<span class="page-link"><i class="pr-0 mdi mdi-chevron-right"></i></span>
					</li>
					<li class="page page-item" id="last" role="button">
						<span class="page-link"><i class="pr-0 mdi mdi-chevron-double-right"></i></span>
					</li>
				</ul>
			</nav>
		</div>
		<div class="d-flex flex-column justify-content-center align-items-center state" id="loading">
			<div class="loader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="pathp" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
				</svg>
			</div>
		</div>
		<div class="compose">
			<a href="{{url('create/user')}}" class="btn btn-primary d-flex align-items-center shadow px-3" style="border-radius:100px">
				<i class="mdi mdi-plus-thick mdi-18px"></i> Tambah User
			</a>
		</div>
	</div>
	<div class="modal" id="modal" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered text-center">
			<div class="modal-content border-0" style="background: transparent;">
				<div class="modal-body">
					<img id="photo">
				</div>	
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script src="{{asset('assets/api/user.js')}}"></script>
	<script>get_user()</script>
@endsection