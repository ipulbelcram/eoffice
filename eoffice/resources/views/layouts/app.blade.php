<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title') | e-office</title>
	<link rel="stylesheet" href="{{asset('assets/vendors/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/loader.css')}}">
	@yield('style')
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-white border-bottom">
        <div class="form-inline">
            <i class="mdi mdi-menu mdi-24px d-block d-lg-none pointer text-dark mr-2" id="menu"></i>
            <a class="navbar-brand" href="{{url('dashboard')}}">
				<img src="{{asset('assets/images/eoffice.png')}}" width="30" class="d-inline-block align-top mr-2" alt="" loading="lazy">
            	e-office
            </a>
        </div>
        <div class="dropdown ml-auto">
            <a id="dropdownMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            	<img src="{{asset('assets/images/photo.png')}}" class="avatar rounded-circle" width="25">
            </a>
            <div class="dropdown-menu dropdown-menu-right rounded" aria-labelledby="dropdownMenu">
            	<div class="text-center my-3 px-3 text-break">
	            	<img src="{{asset('assets/images/photo.png')}}" class="avatar rounded-circle" width="75">
	            	<h6 class="name text-truncate pt-3 mb-0"></h6>
	            	<small class="jabatan text-secondary"></small>
	            </div>
	            <div class="dropdown-divider"></div>
                <a class="dropdown-item {{Request::is('profil')?'active':''}}" href="{{url('profil')}}">
                    <i class="mdi mdi-18px mdi-account-box-outline"></i><span>Profil</span>
                </a>
                <a class="dropdown-item {{Request::is('ubah-password')?'active':''}}" href="{{url('ubah-password')}}">
                    <i class="mdi mdi-18px mdi-lock-outline"></i><span>Ubah Password</span>
                </a>
                <a class="dropdown-item" id="logout" role="button">
                    <i class="mdi mdi-18px mdi-login-variant"></i><span>Keluar</span>
                </a>
            </div>
        </div>
    </nav>
	<div class="sidebar">
		<a href="{{url('dashboard')}}" class="{{Request::is('dashboard')?'active':''}}">
			<i class="mdi mdi-apps mdi-18px"></i><span>Dashboard</span>
		</a>
		@if(session("user_level_id") == 1)
		<a href="{{url('user')}}" class="{{Request::is('user')?'active':''}}">
			<i class="mdi mdi-account-circle-outline mdi-18px"></i><span>User</span>
		</a>
		@else
		<small class="text-secondary text-uppercase font-weight-bold">Surat</small>
		<a href="{{url('surat-masuk')}}" class="{{Request::is('surat-masuk')?'active':''}}">
			<i class="mdi mdi-email-receive-outline mdi-18px"></i><span>Surat Masuk</span>
		</a>
		<a href="{{url('surat-keluar')}}" class="{{Request::is('surat-keluar')?'active':''}}">
			<i class="mdi mdi-email-send-outline mdi-18px"></i><span>Surat Keluar</span>
		</a>
		<!-- <a href="{{url('draft')}}" class="{{Request::is('draft')?'active':''}}">
			<i class="mdi mdi-file-outline mdi-18px"></i><span>Draft</span>
		</a>
		<a href="{{url('sampah')}}" class="{{Request::is('sampah')?'active':''}}">
			<i class="mdi mdi-trash-can-outline mdi-18px"></i><span>Sampah</span>
		</a> -->
		<small class="text-secondary text-uppercase font-weight-bold">Disposisi</small>
		<a href="{{url('disposisi-masuk')}}" class="{{Request::is('disposisi-masuk')?'active':''}}">
			<i class="mdi mdi-clipboard-arrow-left-outline mdi-18px"></i><span>Disposisi Masuk</span>
		</a>
		<a href="{{url('disposisi-keluar')}}" class="{{Request::is('disposisi-keluar')?'active':''}}">
			<i class="mdi mdi-clipboard-arrow-right-outline mdi-18px"></i><span>Disposisi Keluar</span>
		</a>
		<small class="text-secondary text-uppercase font-weight-bold">Informasi</small>
		<a href="{{url('informasi-masuk')}}" class="{{Request::is('informasi-masuk')?'active':''}}">
			<i class="mdi mdi-bullhorn-outline mdi-18px"></i><span>Informasi Masuk</span>
		</a>
		<a href="{{url('informasi-keluar')}}" class="{{Request::is('informasi-keluar')?'active':''}}">
			<i class="mdi mdi-bullhorn-outline mdi-18px"></i><span>Informasi Keluar</span>
		</a>
		<small class="text-secondary text-uppercase font-weight-bold">Tracking</small>
		<a href="{{url('tracking-surat')}}" class="{{Request::is('tracking-surat')?'active':''}}">
			<i class="mdi mdi-email-search-outline mdi-18px"></i><span>Tracking Surat</span>
		</a>
		@endif
	</div>
	<div class="overlay"></div>
	<div class="main">
		@yield('content')
	</div>
	<div class="customAlert"></div>
	<script type="text/javascript" src="{{asset('assets/vendors/jquery/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendors/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/main.js')}}"></script>
	<script type="text/javascript">
		const root = '{{Request::root()}}/'
		const api_url = 'http://103.64.15.49/eoffice/eoffice_api/'
		const token = '{{session("token")}}'
		const id_user = '{{session("id")}}'
	</script>
	<script type="text/javascript" src="{{asset('assets/api/auth/session.js')}}"></script>
	@yield('script')
</body>
</html>