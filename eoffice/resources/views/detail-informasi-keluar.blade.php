@extends('layouts/app')

@section('style')
	<style type="text/css">
		td {
			vertical-align: top !important;
		}
		td:first-child {
			color: #607070 !important;
		}
		td:nth-child(2) {
			color: #607070 !important;
			padding: 0 10px;
		}
	</style>
@endsection

@section('content')
	<div class="container mb-5">
		<div class="d-flex mb-3">
			<a href="{{url('informasi-keluar')}}" class="mr-auto text-dark"><i class="mdi mdi-arrow-left mdi-18px"></i></a>
		</div>
		<div class="clearfix">
			<img src="{{asset('assets/images/photo.png')}}" class="rounded-circle float-left mr-3" id="foto">
			<div class="d-inline-flex flex-column">
				<span id="sender"></span>
				<small role="button">Detail <i class="mdi mdi-chevron-up"></i></small>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6 col-lg-7 col-md-8">
				<div class="card rounded mt-3">
					<div class="card-body py-3">
						<table>
							<tbody>
								<tr>
									<td>Nomor Informasi</td>
									<td>:</td>
									<td id="no_surat"></td>
								</tr>
								<tr>
									<td>Asal Informasi</td>
									<td>:</td>
									<td id="asal_surat"></td>
								</tr>
								<tr>
									<td>Tanggal Informasi</td>
									<td>:</td>
									<td id="tanggal_surat"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<hr class="mt-4">
		<div class="mb-2">
			<i class="mdi mdi-attachment mdi-rotate-135 pr-0"></i> <span id="total-file"></span> Lampiran
		</div>
		<div class="row" id="file"></div>
		<hr>
		<div class="mt-3">
			<span>History Informasi</span>
			<div class="mt-2" id="log-surat"></div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">const id = '{{Request::route("id")}}'</script>
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/detail-informasi-keluar.js')}}"></script>
@endsection