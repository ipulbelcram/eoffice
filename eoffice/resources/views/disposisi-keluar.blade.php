@extends('layouts/app')

@section('title','Disposisi Keluar')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between none-i" id="head">
			<p class="text-secondary text-uppercase">Disposisi Keluar</p>
			<div class="dropdown">
				<p class="text-secondary" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="mdi mdi-filter-outline"></i>Filter
				</p>
				<div class="dropdown-menu dropdown-menu-right">
					<h6 class="dropdown-header text-uppercase pl-3">Jenis Surat</h6>
					<button class="dropdown-item dropdown-filter active" data-id="0">Semua Jenis Surat</button>
					<button class="dropdown-item dropdown-filter" data-id="1">Memorandum</button>
					<button class="dropdown-item dropdown-filter" data-id="2">Permohonan Narasumber</button>
					<button class="dropdown-item dropdown-filter" data-id="3">Undangan Rapat/Acara</button>
					<button class="dropdown-item dropdown-filter" data-id="4">Proposal Kegiatan</button>
					<button class="dropdown-item dropdown-filter" data-id="5">Surat Edaran/Pemberitahuan</button>
				</div>
			</div>
		</div>
		<!-- <hr class="mx-3 my-1 d-none d-md-block"> -->
		<div class="box-container mb-5" id="data"></div>
		<div class="d-flex flex-column justify-content-center align-items-center none-i" id="empty" style="height:60vh;">
			<img src="{{asset('assets/images/empty.svg')}}" width="150">
			<h6 class="font-weight-normal text-secondary mt-4">Disposisi Keluar kosong</h6>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/disposisi-keluar.js')}}"></script>
@endsection