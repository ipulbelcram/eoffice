@extends('layouts/app')

@section('title','Surat Masuk')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between none-i" id="head">
			<p class="text-secondary text-uppercase">Surat Masuk</p>
			<div class="dropdown">
				<!-- butuh api id user untuk filter -->
				<!-- <p class="text-secondary" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="mdi mdi-filter-outline"></i>Filter
				</p> -->
				<div class="dropdown-menu dropdown-menu-right">
					<h6 class="dropdown-header text-uppercase pl-3">Jenis Surat</h6>
					<button class="dropdown-item dropdown-filter active" data-id="0">Semua Jenis Surat</button>
					<button class="dropdown-item dropdown-filter" data-id="1">Memorandum</button>
					<button class="dropdown-item dropdown-filter" data-id="2">Permohonan Narasumber</button>
					<button class="dropdown-item dropdown-filter" data-id="3">Undangan Rapat/Acara</button>
					<button class="dropdown-item dropdown-filter" data-id="4">Proposal Kegiatan</button>
					<button class="dropdown-item dropdown-filter" data-id="5">Surat Edaran/Pemberitahuan</button>
				</div>
			</div>
		</div>
		<!-- <hr class="mx-3 my-1 d-none d-md-block"> -->
		<div class="box-container mb-5" id="data"></div>
		<div class="d-flex flex-column justify-content-center align-items-center none-i" id="empty" style="height:60vh">
			<img src="{{asset('assets/images/empty.svg')}}" width="150">
			<h6 class="font-weight-normal text-secondary mt-4">Surat Masuk kosong</h6>
		</div>
		<div class="compose">
			<a href="{{url('tambah/surat-masuk')}}" class="btn btn-sm btn-primary d-flex align-items-center shadow px-3" style="border-radius:100px">
				<i class="mdi mdi-pencil-outline mdi-18px"></i> Tambah Surat
			</a>
		</div>
	</div>
@endsection

@section('script')
	<script>const id = '{{Request::route("id")}}'</script>
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/user-surat-masuk.js')}}"></script>
@endsection