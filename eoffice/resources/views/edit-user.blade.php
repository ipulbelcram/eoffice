@extends('layouts/app')

@section('title','Edit User')

@section('style')
	<link rel="stylesheet" href="{{asset('assets/vendors/croppie/croppie.css')}}">
	<style type="text/css">
		.password {
			position: absolute;
			right: 15px;
			top: 0px;
			cursor: pointer;
			color: rgb(0,0,0,0.5);
			padding: 8px 10px;
		}
		.password.invalid {
			right: 40px;
		}
	</style>
@endsection

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Edit User</p>
		<!-- <div class="form-row align-items-center mt-4 pb-3">
			<div class="col-xl-3 col-lg-4 col-sm-5 d-none d-sm-block">
				<label>Foto Profil</label>
			</div>
			<div class="col-xl-5 col-lg-6 col-sm-7 text-center text-sm-left">
				<div class="d-inline-block" data-toggle="modal" data-target="#modalAvatar" role="button">
					<img src="{{asset('assets/images/photo.png')}}" class="avatar rounded-circle" width="100"><br>
					<div class="btn btn-link btn-sm ml-sm-2" data-toggle="modal" data-target="#modalAvatar">Ubah Foto</div>
				</div>
			</div>
		</div> -->
		<form id="form">
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nama Lengkap</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="name">
					<div class="invalid-feedback" id="name-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="email" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Email</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input type="email" class="form-control" id="email">
					<div class="invalid-feedback" id="email-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="jabatan" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Jabatan</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input type="email" class="form-control" id="jabatan" disabled="disabled">
				</div>
			</div>

			<hr class="mt-4">
			<p class="text-secondary text-uppercase">Informasi Pribadi</p>
			<div class="form-group row">
				<label for="username" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Username</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="username" disabled="disabled">
				</div>
			</div>
			<div class="form-group row">
				<label for="nip" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">NIP</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<input class="form-control" id="nip" minlength="18" maxlength="18">
					<div class="invalid-feedback" id="nip-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="gender" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Jenis Kelamin</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 pt-1">
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="gender" id="male" value="laki-laki">
						<label class="form-check-label" for="male" role="button">Laki-Laki</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="gender" id="female" value="perempuan">
						<label class="form-check-label" for="female" role="button">Perempuan</label>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="phone" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Nomor Telepon</label>
				<div class="col-xl-5 col-lg-6 col-sm-7">
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text" id="plus">
								<small>+62</small>
							</div>
						</div>
						<input type="tel" class="form-control" id="phone" aria-describedby="plus">
					</div>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit">
						<span id="load" class="none"><i class="mdi mdi-spin mdi-loading pr-0"></i></span>
						<span id="text">Simpan</span>
					</button>
				</div>
			</div>
		</form>
		<hr class="mt-4">
		<p class="text-secondary text-uppercase">Reset Password</p>
		<form id="reset-password">
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Password Baru</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="npassword" minlength="8" maxlength="32" autocomplete="autocomplete">
					<div class="invalid-feedback" id="npassword-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="npassword"></i>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-xl-3 col-lg-4 col-sm-5 col-form-label">Konfirmasi Password</label>
				<div class="col-xl-5 col-lg-6 col-sm-7 position-relative">
					<input type="password" class="form-control" id="cpassword" minlength="8" maxlength="32" autocomplete="autocomplete">
					<div class="invalid-feedback" id="cpassword-feedback"></div>
					<i class="password mdi mdi-eye-off" data-id="cpassword"></i>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit-password">
						<span id="load-password" class="none"><i class="mdi mdi-spin mdi-loading pr-0"></i></span>
						<span id="text-password">Reset Password</span>
					</button>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
	<script>const id = '{{Request::route("id")}}'</script>
	<script src="{{asset('assets/api/edit-user.js')}}"></script>
	<script src="{{asset('assets/api/reset-password.js')}}"></script>
@endsection