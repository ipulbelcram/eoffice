@extends('layouts/app')

@section('content')
	<div class="container">
		<div class="d-flex align-items-center mb-3">
			<a href="{{url('surat-masuk')}}" class="text-dark"><i class="mdi mdi-arrow-left mdi-18px"></i></a>
			<div class="none ml-auto" id="action">
				<a class="text-dark mr-4" id="edit"><i class="mdi mdi-18px mdi-pencil-outline pr-0"></i></a>
				<i class="mdi mdi-18px mdi-trash-can-outline pr-0" role="button" data-toggle="modal" data-target="#modal-delete"></i>
			</div>
		</div>
		<div class="clearfix">
			<img src="{{asset('assets/images/photo.png')}}" width="40" class="rounded-circle float-left mr-3" id="foto">
			<div class="d-inline-flex flex-column">
				<span id="sender"></span>
				<small role="button">Detail <i class="mdi mdi-chevron-up"></i></small>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6 col-lg-7 col-md-8">
				<div class="card rounded mt-3">
					<div class="card-body py-3">
						<table>
							<tbody>
								<tr>
									<td>Nomor Agenda</td>
									<td>:</td>
									<td id="no_agenda"></td>
								</tr>
								<tr>
									<td>Nomor Surat</td>
									<td>:</td>
									<td id="no_surat"></td>
								</tr>
								<tr>
									<td>Asal Surat</td>
									<td>:</td>
									<td id="asal_surat"></td>
								</tr>
								<tr>
									<td>Tanggal Surat</td>
									<td>:</td>
									<td id="tanggal_surat"></td>
								</tr>
								<tr>
									<td>Jenis Surat</td>
									<td>:</td>
									<td id="jenis_surat"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="mt-3">
			<span class="text-secondary">Perihal :</span>
			<p id="perihal"></p>
		</div>
		<hr>
		<div class="mb-2">
			<i class="mdi mdi-attachment mdi-rotate-135 pr-0"></i> <span id="total-file"></span> Lampiran
		</div>
		<div class="row" id="file"></div>
		<hr>
		<div class="mt-3">
			<span>History Surat</span>
			<div class="mt-2" id="log-surat"></div>
		</div>
		
		<div class="row pt-5">
			<div class="btn-forward none col-xl-3 col-lg-4 mb-2">
				<div class="btn btn-block btn-outline-primary" data-toggle="modal" data-target="#modal-teruskan">
					Teruskan Surat
				</div>
			</div>
			<div class="btn-disposition none col-xl-3 col-lg-4 mb-2">
				<div class="btn btn-block btn-primary" data-toggle="modal" data-target="#modal-disposisi">
					Disposisikan Surat
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-delete" tabindex="-1" aria-hidden="true">
		<div class="modal-sm modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title">Hapus Surat?</h5>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body py-0">
					<span>Semua surat yang telah <b>diteruskan</b> & <b>didisposisikan</b> akan ikut terhapus.</span>
				</div>
				<div class="modal-footer border-top-0">
					<div class="btn btn-sm btn-link" data-dismiss="modal">Batal</div>
					<button class="btn btn-sm btn-link" id="delete">Hapus</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-teruskan" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title">Teruskan Surat</h5>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body h-100">
					<form id="teruskan">
						<div class="form-group">
							<label>Penerima</label>
							<div class="form-control" data-toggle="modal" data-target="#modal-reciver-t" role="button">
								<span class="text-secondary" id="total-reciver-t" style="font-size:14px">Pilih Penerima</span>
							</div>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control form-control-sm" rows="3" id="keteranganTeruskan"></textarea>
						</div>
						<div class="text-right pt-3">
							<div class="btn btn-sm btn-link px-4" data-dismiss="modal">Batal</div>
							<button class="btn btn-sm btn-primary px-4" id="submit-t">
								<span id="load-t" class="none"><i class="mdi mdi-spin mdi-loading"></i></span>Kirim Surat
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-disposisi" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable" role="document">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title">Form Disposisi</h5>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body">
					<form id="disposisi">
						<div class="form-group">
							<label>Penerima</label>
							<div class="form-control" data-toggle="modal" data-target="#modal-reciver" role="button">
								<span class="text-secondary" id="total-reciver" style="font-size:14px">Pilih Penerima</span>
							</div>
						</div>
						<div class="form-group" id="instruksi">
							<label class="mb-0">Instruksi Disposisi</label>
						</div>
						<div class="form-group">
							<lable>Sifat</lable>
							<div class="row">
								<div class="col-6" id="sifat1"></div>
								<div class="col-6" id="sifat2"></div>
							</div>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control form-control-sm" rows="3" id="keterangan"></textarea>
						</div>
						<div class="text-right pt-3">
							<div class="btn btn-sm btn-link px-4" data-dismiss="modal">Batal</div>
							<button class="btn btn-sm btn-primary px-4" id="submit">
								<span id="load" class="none"><i class="mdi mdi-spin mdi-loading"></i></span>Kirim Disposisi
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-reciver-t" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h6 class="modal-title text-capitalize">Pilih Penerima</h6>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#modal-teruskan">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body py-0">
					<div class="form-group">
						<input class="form-control" id="search-reciver-t" placeholder="Cari...">
					</div>
					<div id="reciver-t" class="text-truncate overflow-auto" style="height:235px">
						<div id="empty-t" class="text-center text-secondary none">Data tidak ditemukan.</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-link px-4" data-dismiss="modal" data-toggle="modal" data-target="#modal-teruskan">Batal</button>
					<button type="button" class="btn btn-sm btn-primary px-4" id="btn-select-t" data-dismiss="modal">Pilih Penerima</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-reciver" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h6 class="modal-title text-capitalize">Pilih Penerima</h6>
					<div role="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#modal-disposisi">
						<i class="mdi mdi-close mdi-18px pr-0"></i>
					</div>
				</div>
				<div class="modal-body py-0">
					<div class="form-group">
						<input class="form-control" id="search-reciver" placeholder="Cari...">
					</div>
					<div id="reciver" class="text-truncate overflow-auto" style="height:235px">
						<div id="empty" class="text-center text-secondary none">Data tidak ditemukan.</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-link px-4" data-dismiss="modal" data-toggle="modal" data-target="#modal-disposisi">Batal</button>
					<button type="button" class="btn btn-sm btn-primary px-4" id="btn-select" data-dismiss="modal">Pilih Penerima</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">const id = '{{Request::route("id")}}'</script>
	<script type="text/javascript">const id_terkirim = '{{Request::route("id_terkirim")}}'</script>
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/add-reciver-teruskan.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/add-reciver-disposisi.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/detail-surat-masuk.js')}}"></script>
@endsection