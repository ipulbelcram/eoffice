@extends('layouts/app')

@section('title','Tracking Surat')

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Tracking Surat</p>
		<form id="form" class="pt-2">
			<div class="form-group">
				<div class="input-group mb-3">
					<input type="search" id="search" class="form-control" placeholder="Nomor Surat" autofocus="autofocus">
					<div class="input-group-append">
						<button class="btn btn-sm btn-primary px-3" id="submit">Cari</button>
					</div>
				</div>
			</div>
		</form>
		<div class="box-container none mt-3" id="data"></div>
		<div class="text-center none" id="empty" style="padding-top:100px">
			<img src="{{asset('assets/images/tracking.svg')}}" width="150">
			<h6 class="font-weight-normal text-secondary mt-4">Surat tidak ditemukan</h6>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/tracking-surat.js')}}"></script>
@endsection