@extends('layouts/app')

@section('title','History')

@section('content')
	<div class="container">
		<div id="log-surat">
			<p class="text-secondary text-uppercase d-flex align-items-center">
				<i class="mdi mdi-arrow-left mdi-18px pr-3" role="button" onclick="return history.back()"></i>History
			</p>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">const id = '{{Request::route("id")}}'</script>
	<script type="text/javascript" src="{{asset('assets/js/date.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/history.js')}}"></script>
@endsection