@extends('layouts/app')

@section('title','Dashboard')

@section('content')
	<div class="container">
		<h5 class="name">Halo, </h5>
		<p class="text-secondary">Selamat bergabung di e-office.</p>
		<div class="row pt-2 mb-3">
			@if(session("user_level_id") == 1)
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('user')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>User</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-account-circle-outline mdi-36px"></i>
								<h4 class="mb-0" id="user">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
								<div class="notification none"></div>
							</div>
						</div>
					</div>
				</a>
			</div>
			@else
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('surat-masuk')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Surat Masuk</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-email-receive-outline mdi-36px"></i>
								<h4 class="mb-0" id="surat-masuk">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
								<div class="notification none"></div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('surat-keluar')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Surat Keluar</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-email-send-outline mdi-36px"></i>
								<h4 class="mb-0" id="surat-keluar">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
								<div class="notification none"></div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('disposisi-masuk')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Disposisi Masuk</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-clipboard-arrow-left-outline mdi-36px"></i>
								<h4 class="mb-0" id="disposisi-masuk">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('disposisi-keluar')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Disposisi Keluar</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-clipboard-arrow-right-outline mdi-36px"></i>
								<h4 class="mb-0" id="disposisi-keluar">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('informasi-masuk')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Informasi Masuk</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-bullhorn-outline mdi-36px"></i>
								<h4 class="mb-0" id="informasi-masuk">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('informasi-keluar')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Informasi Keluar</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-bullhorn-outline mdi-36px"></i>
								<h4 class="mb-0" id="informasi-keluar">
									<div class="loader btn-loading">
										<svg class="circular" viewBox="25 25 50 50">
											<circle class="pathd" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
										</svg>
									</div>
								</h4>
							</div>
						</div>
					</div>
				</a>
			</div>
			@endif
		</div>
		@if(session("user_level_id") != 1)
		<p class="text-secondary text-uppercase font-weight-bold">Tracking</p>
		<div class="row">
			<div class="col-6 col-md-4 mb-4">
				<a href="{{url('tracking-surat')}}">
					<div class="card card-menu rounded">
						<div class="card-body">
							<h6>Tracking Surat</h6>
							<div class="d-flex justify-content-between align-items-center position-relative">
								<i class="mdi mdi-email-search-outline mdi-36px"></i>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<footer class="d-flex flex-column text-secondary border-top mt-5 pt-3">
			<small>Copyright &copy; 2020</small>
			<small>Sesdep Bidang Pengembangan SDM - Kementerian KUKM RI</small>
			<small>Versi 1.0</small>
		</footer>
		@endif
	</div>
@endsection

@section('script')
	@if(session("user_level_id") == 1)
	<script type="text/javascript" src="{{asset('assets/api/user.js')}}"></script>
	<script>get_total()</script>
	@else
	<script type="text/javascript" src="{{asset('assets/api/dashboard.js')}}"></script>
	@endif
@endsection