<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title') | e-office</title>
	<link rel="stylesheet" href="{{asset('assets/vendors/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	@yield('style')
</head>
<body>
	<div class="auth">
		@yield('content')
	</div>
	<script type="text/javascript" src="{{asset('assets/vendors/jquery/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendors/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/main.js')}}"></script>
	<script type="text/javascript">
		const root = '{{Request::root()}}/'
		const api_url = 'http://103.64.15.49/eoffice/eoffice_api/'
	</script>
	@yield('script')
</body>
</html>