@extends('auth/layouts/app')

@section('title','Login')

@section('content')
	<div class="card rounded">
		<div class="card-head text-center px-4 pt-4">
			<img src="{{asset('assets/images/eoffice.png')}}" width="120">
			<h2 class="pt-4">e-office</h2>
			<p class="text-secondary mb-0">Deputi Bidang Pengembangan SDM</p>
			<p class="text-secondary">Kementerian Koperasi dan Usaha Kecil dan Menengah</p>
		</div>
		<div class="card-body">
			<form id="form">
				<div class="form-group">
					<label for="username">Username</label>
					<input type="text" id="username" class="form-control" autofocus="autofocus">
					<div class="invalid-feedback">Username atau password salah.</div>
				</div>
				<div class="form-group position-relative">
					<label for="password">Password</label>
					<input type="password" id="password" class="form-control pr-5" maxlength="32" autocomplete="on">
					<i class="password mdi mdi-eye-off mdi-18px" data-id="password"></i>
				</div>
				<div class="form-group mt-5">
					<button class="btn btn-primary btn-block mb-4" id="submit">
						<span id="text">Login</span>
						<span id="load" class="none"><i class="mdi mdi-spin mdi-loading pr-0"></i></span>
					</button>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/api/auth/login.js')}}"></script>
@endsection