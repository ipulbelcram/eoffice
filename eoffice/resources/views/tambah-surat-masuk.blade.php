@extends('layouts/app')

@section('title','Tambah Surat')

@section('style')
	<style type="text/css">
		.form-check {
			padding-bottom: 5px;
		}
	</style>
@endsection

@section('content')
	<div class="container">
		<p class="text-secondary text-uppercase">Tambah Surat</p>
		<form id="form" enctype="multipart/form-data">
			<div class="form-group row">
				<label for="no_surat" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Nomor Surat</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<input class="form-control" id="no_surat" autofocus="autofocus">
					<div class="invalid-feedback" id="no_surat-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="asal_surat" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Asal Surat</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<input class="form-control" id="asal_surat">
					<div class="invalid-feedback" id="asal_surat-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="tanggal_surat" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Tanggal Surat</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<input type="date" class="form-control" id="tanggal_surat">
					<div class="invalid-feedback" id="tanggal_surat-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="jenis_surat_id" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Jenis Surat</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<div class="form-check">
						<input name="jenis_surat_id" class="form-check-input" type="radio" id="memorandum" value="1">
						<label class="form-check-label" for="memorandum">Memorandum</label>
					</div>
					<div class="form-check">
						<input name="jenis_surat_id" class="form-check-input" type="radio" id="permohonan" value="2">
						<label class="form-check-label" for="permohonan">Permohonan Narasumber</label>
					</div>
					<div class="form-check">
						<input name="jenis_surat_id" class="form-check-input" type="radio" id="undangan" value="3">
						<label class="form-check-label" for="undangan">Undangan Rapat/Acara</label>
					</div>
					<div class="form-check">
						<input name="jenis_surat_id" class="form-check-input" type="radio" id="proposal" value="4">
						<label class="form-check-label" for="proposal">Proposal Kegiatan</label>
					</div>
					<div class="form-check">
						<input name="jenis_surat_id" class="form-check-input" type="radio" id="surat" value="5">
						<label class="form-check-label" for="surat">Surat Edaran/Pemberitahuan</label>
					</div>
					<div class="small text-danger" id="jenis_surat_id-feedback"></div>
				</div>
			</div>
			<div class="form-group row">
				<label for="perihal" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Perihal</label>
				<div class="col-xl-5 col-lg-6 col-md-7">
					<textarea class="form-control form-control-sm" rows="4" id="perihal" placeholder="Perihal"></textarea>
					<div class="invalid-feedback" id="perihal-feedback"></div>
				</div>
			</div>
			<div class="form-group row mb-0">
				<label for="lampiran" class="col-xl-3 col-lg-4 col-md-5 col-form-label">Lampiran</label>
				<div class="col-xl-5 col-lg-6 col-md-7" id="form-file">
					<div class="file-group mb-3">
						<div class="custom-file" data-index="0">
							<input type="file" class="file custom-file-input" role="button">
							<label class="custom-file-label">Pilih Lampiran</label>
							<div class="invalid-feedback"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<div class="small text-danger pt-1" id="file-feedback"></div>
				</div>
			</div>
			<div class="form-group row mt-5 mb-sm-5">
				<div class="offset-xl-3 offset-lg-4 offset-md-5 col-xl-5 col-lg-6 col-md-7">
					<button class="btn btn-primary btn-block" id="submit">
						<div class="loader none" id="load">
							<svg class="circular" viewBox="25 25 50 50">
								<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="6" stroke-miterlimit="1"/>
							</svg>
						</div>
						<span id="text">Simpan Surat</span>
					</button>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('assets/api/add-file.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/api/tambah-surat-masuk.js')}}"></script>
@endsection