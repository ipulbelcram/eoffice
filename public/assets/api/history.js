$.ajax({
	url: api_url+'log_history/detail/'+id,
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		let value = result.data
		let appendLog = ''
		let length = value.reciver.length
		let log,mdi

			appendLog +=
			`<div class="row mb-2">
		        <div class="col-auto text-center flex-column d-sm-flex px-1">
		            <h5 class="m-2">
		                <i class="mdi mdi-check-all mdi-18px text-primary pr-0"></i>
		            </h5>
		            <div class="row h-50">
		                <div class="col border-right">&nbsp;</div>
		                <div class="col">&nbsp;</div>
		            </div>
		        </div>
		        <div class="col col-xl-10 pl-0 pt-2">
		        	<div class="d-flex flex-column align-items-start">
		            	<small>`+value.sender.name+`</small>
		            	<small>Mengirim</small>
		            </div>
		        </div>
		    </div>`

		$.each(value.reciver, function(index, value) {
			let ket = (value.keterangan == null) ? 'Tidak ada keterangan' : value.keterangan
			let border = (index != (length-1)) ? 'border-right' : ''
			switch(value.status_message) {
				case 'sent' :
					mdi = 'mdi-check'
					log = 'Terkirim'
					break
				case 'read' :
					mdi = 'mdi-check-all text-primary'
					log = 'Dilihat'
			}
			appendLog +=
			`<div class="row mb-2">
		        <div class="col-auto text-center flex-column d-sm-flex px-1">
		            <h5 class="m-2">
		                <i class="mdi `+mdi+` mdi-18px pr-0"></i>
		            </h5>
		            <div class="row h-50">
		                <div class="col `+border+`">&nbsp;</div>
		                <div class="col">&nbsp;</div>
		            </div>
		        </div>
		        <div class="col col-xl-10 pl-0 pt-2">
		        	<div class="d-flex flex-column align-items-start">
		            	<small>`+value.name+`</small>
		            	<small>`+log+` - `+ket+`</small>
		            </div>
		        </div>
		    </div>`
		})
		$('#log-surat').append(appendLog)
	}
})