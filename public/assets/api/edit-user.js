$.ajax({
    url: api_url + 'user/detail/' + id,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        // console.log(value)
        $('#name').val(value.name)
        $('#email').val(value.email)
        $('#jabatan').val(value.jabatan_name)
        $('#username').val(value.username)
        $('#nip').val(value.nip)
        value.no_hp != 0 ? $('#phone').val(value.no_hp) : ''
        if (value.jenis_kelamin == "laki-laki") {
            $('#male').attr('checked', true)
        } else if (value.jenis_kelamin == "perempuan") {
            $('#female').attr('checked', true)
        }
    }
})

$('#form').submit(function(e) {
    addLoading()
    e.preventDefault()
    $('.form-control').removeClass('is-invalid')

    let name = $('#name').val()
    let email = $('#email').val()
    let nip = $('#nip').val()
    let gender = $('input[type=radio][name=gender]:checked').val()
    let phone = $('#phone').val()

    $.ajax({
        url: api_url + 'user/update/' + id,
        type: 'PATCH',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        data: {
            name: name,
            email: email,
            nip: nip,
            jenis_kelamin: gender,
            no_hp: phone
        },
        success: function(result) {
        	location.href = root + 'user'
        },
        error: function(xhr) {
        	removeLoading()
            let err = JSON.parse(xhr.responseText)
            // console.log(err)
            if (err.name) {
                $('#name').addClass('is-invalid')
                $('#name-feedback').html("Masukkan nama lengkap.")
            }
            if (err.email) {
                $('#email').addClass('is-invalid')
                $('#email-feedback').html("Email telah digunakan.")
            }
            if (err.nip) {
	            if (err.nip == "The nip field is required.") {
	                $('#nip').addClass('is-invalid')
	                $('#nip-feedback').html("Masukkan NIP.")
	            }
	            else if (err.nip == "The nip has already been taken.") {
	                $('#nip').addClass('is-invalid')
	                $('#nip-feedback').html("NIP telah digunakan.")
	            }
            }
        }
    })
})