function get_total() {
   $.ajax({
      url: api_url + "user",
      type: "GET",
      beforeSend: function (xhr) {
         xhr.setRequestHeader("Authorization", "Bearer " + token);
      },
      success: function (result) {
         $("#user").html(result.meta.total);
      },
   });
}

function get_user(page) {
   $.ajax({
      url: api_url + "user",
      type: "GET",
      data: {
         page: page,
      },
      beforeSend: function (xhr) {
         xhr.setRequestHeader("Authorization", "Bearer " + token);
      },
      success: function (result) {
         // console.log(result.data)
         $("#pagination").show();
         $("#loading_table").hide();
         $("#loading").addClass("none-i");
         if (result.data.length > 0) {
            $("#data").removeClass("none-i");
            $.each(result.data, function (index, value) {
               append = `<tr>
						<th><i class="mdi mdi-check mdi-checkbox-blank-outline mdi-18px pr-0" role="button"></i></th>
						<td><a href="${root}user/${value.id}">${value.name}</a></td>
						<td>${value.jabatan}</td>
						<td><img src="${value.foto}" class="rounded-circle photo" width="30" role="button"></td>
						<td class="text-center"><a href="${root}user/${value.id}/surat-masuk">${value.total_surat_masuk}</a></td>
					</tr>`;
               $("#table").append(append);
            });

            let links = result.links;
            let meta = result.meta;
            let current = meta.current_page;
            let replace = "http://103.64.15.49/eoffice/eoffice_api/user?page=";

            let first = links.first.replace(replace, "");
            if (first != current) {
               $("#first").removeClass("disabled");
               $("#first").data("id", first);
            } else {
               $("#first").addClass("disabled");
            }

            if (links.prev != null) {
               $("#prev").removeClass("disabled");
               let prev = links.prev.replace(replace, "");
               $("#prev").data("id", prev);

               $("#prevCurrent").show();
               $("#prevCurrent span").html(prev);
               $("#prevCurrent").data("id", prev);

               let prevCurrentDouble = prev - 1;
               if (prevCurrentDouble > 0) {
                  $("#prevCurrentDouble").show();
                  $("#prevCurrentDouble span").html(prevCurrentDouble);
                  $("#prevCurrentDouble").data("id", prevCurrentDouble);
               } else {
                  $("#prevCurrentDouble").hide();
               }
            } else {
               $("#prev").addClass("disabled");
               $("#prevCurrent").hide();
               $("#prevCurrentDouble").hide();
            }

            $("#current").addClass("active");
            $("#current span").html(current);

            if (links.next != null) {
               $("#next").removeClass("disabled");
               let next = links.next.replace(replace, "");
               $("#next").data("id", next);

               $("#nextCurrent").show();
               $("#nextCurrent span").html(next);
               $("#nextCurrent").data("id", next);

               let nextCurrentDouble = ++next;
               if (nextCurrentDouble <= meta.last_page) {
                  $("#nextCurrentDouble").show();
                  $("#nextCurrentDouble span").html(nextCurrentDouble);
                  $("#nextCurrentDouble").data("id", nextCurrentDouble);
               } else {
                  $("#nextCurrentDouble").hide();
               }
            } else {
               $("#next").addClass("disabled");
               $("#nextCurrent").hide();
               $("#nextCurrentDouble").hide();
            }

            let last = links.last.replace(replace, "");
            if (last != current) {
               $("#last").removeClass("disabled");
               $("#last").data("id", last);
            } else {
               $("#last").addClass("disabled");
            }
         } else {
            $("#empty").removeClass("none-i");
         }
      },
      error: function (xhr, status) {
         setTimeout(function () {
            get_user(page);
         }, 1000);
      },
   });
}

$(".page").click(function () {
   if (!$(this).is(".active, .disabled")) {
      let page = $(this).data("id");
      $("#table").html("");
      $("#pagination").hide();
      $("#loading_table").show();
      get_user(page);
   }
});

$(document).on("click", ".photo", function () {
   let src = $(this).attr("src");
   $("#modal").modal("show");
   $("#photo").attr("src", src);
});
