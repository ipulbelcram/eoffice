$('#form').submit(function(e){
	e.preventDefault()
	$('.password').removeClass('invalid')
	$('#password').removeClass('is-invalid')
	$('#npassword').removeClass('is-invalid')
	$('#cpassword').removeClass('is-invalid')

	$('#text').hide()
	$('#load').show()
	$('#submit').attr('disabled',true)

	let password = $('#password').val()
	let npassword = $('#npassword').val()
	let cpassword = $('#cpassword').val()

	$.ajax({
		url: api_url+'auth/password_update',
		type: 'PATCH',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			old_password: password,
			password: npassword,
			password_confirmation: cpassword
		},
		success: function(result) {
			$('#password').val('')
			$('#npassword').val('')
			$('#cpassword').val('')
			customAlert('<i class="mdi mdi-check-bold"></i>Password berhasil diubah.')
		},
		error: function(xhr) {
			let err = JSON.parse(xhr.responseText)
			// console.log(err)
			$('.password').addClass('invalid')
			if(err.old_password) {
				if(err.old_password == 'The old password field is required.') {
					$('#password').addClass('is-invalid')
					$('#password-feedback').html('Masukkan password lama.')
				}
				else if(err.old_password == 'make sure your fill your current password ') {
					$('#password').addClass('is-invalid')
					$('#password-feedback').html('Password lama salah.')
				}
			}
			if(err.password) {
				if(err.password == 'The password field is required.') {
					$('#npassword').addClass('is-invalid')
					$('#npassword-feedback').html('Masukkan password baru.')
				}
				else if(err.password == 'The password must be at least 8 characters.') {
					$('#npassword').addClass('is-invalid')
					$('#npassword-feedback').html('Password minimal 8 karakter.')
				}
				else if(err.password == 'The password confirmation does not match.') {
					$('#cpassword').addClass('is-invalid')
					$('#cpassword-feedback').html('Konfirmasi password dengan benar.')
				}
			}
			if(err.password_confirmation) {
				if(err.password_confirmation == 'The password confirmation field is required.') {
					$('#cpassword').addClass('is-invalid')
					$('#cpassword-feedback').html('Masukkan konfirmasi password.')
				}
			}
		},
		complete: function() {
			$('#text').show()
			$('#load').hide()
			$('#submit').attr('disabled',false)
		}
	})
})