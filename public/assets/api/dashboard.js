$.ajax({
    url: api_url + 'total_document',
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        $('#surat-masuk').html(value.surat_masuk)
        $('#surat-keluar').html(value.surat_keluar)
        $('#disposisi-masuk').html(value.disposisi_masuk)
        $('#disposisi-keluar').html(value.disposisi_keluar)
        $('#informasi-masuk').html(value.informasi_masuk)
        $('#informasi-keluar').html(value.informasi_keluar)
    }
})