if(localStorage.getItem('send') == 'true') {
	customAlert('<i class="mdi mdi-check"></i>Informasi berhasil terkirim.')
	localStorage.setItem('send',null)
}

$.ajax({
	url: api_url+'informasi/informasi_keluar',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		// console.log(result)
		if(result.total_informasi_keluar != 0) {
			let photo
			$('#head').removeClass('none-i')
			$.each(result.data, function(index, value) {
				photo = (value.sender.foto == '') ? root+'public/assets/images/photo.png' : value.sender.foto
				$('#data').append(
					appendData(
						value.surat.id,
						value.sender.name,
						value.surat.asal_surat,
						value.surat.no_surat,
						value.surat.created_at,
						photo
					)
				)
			})
		} else {
			$('#empty').removeClass('none-i')
		}
	}
})

function appendData(id, sender, asal_surat, no_surat, created_at, photo) {
	let append =
	`<a href="${root}informasi-keluar/${id}">
		<div class="box-row clearfix">
			<small class="box-date-sm">${shortDate(new Date(created_at))}</small>
			<img src="${photo}" width="30" class="float-left rounded-circle mr-3">
			<div class="d-flex flex-column flex-xl-row align-items-base align-items-xl-center mt-xl-1">
				<div class="box-title text-truncate col-xl-3">${sender}</div>
				<div class="box-desc text-truncate col-xl-8 pr-0 pr-xl-3">
					<span class="">${asal_surat}</span>
					<span class="text-secondary d-none d-xl-inline"> - ${no_surat}</span>
				</div>
				<span class="text-truncate text-secondary d-block d-xl-none">${no_surat}</span>
			</div>
		</div>
	</a>`
	return append
}