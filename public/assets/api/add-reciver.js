let reciver_id = []
$.ajax({
	url: api_url+'user/user_reciver',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		let append = ''
		$.each(result.data, function(index, value) {
			append += 
			`<div class="select-reciver rounded p-2 mb-2" data-id="`+value.id+`" data-name="`+value.name.toLowerCase()+`" role="button">
				<i class="mdi mdi-checkbox-blank-outline mdi-18px float-left"></i>
				<div style="margin-top:2px">
					<div class="font-weight-bold text-uppercase text-truncate">`+value.name+`</div>
					<div class="small">`+value.jabatan+`</div>
				</div>
			</div>`
			reciver_id[index] = {'id': value.id, 'name': value.name, 'jabatan': value.jabatan}
		})
		$('#reciver').append(append)
	}
})

$('#search-reciver').keyup(function(){
	let val = $(this).val()
	search(val)
})
function search(val){
	$('#empty').hide()
	if(val.length > 0) {
		$('.select-reciver').each(function(e){
			$(this).hide()
		})
		let empty = 0
		$("#reciver .select-reciver").filter(function() {
			if($(this).data('name').toLowerCase().indexOf(val) > -1){
				$(this).toggle()
			} else {
				empty++
			}
		})
		if(empty == 7) {
			$('#empty').show()
		}
	} else {
		$('.select-reciver').each(function(e){
			$(this).show()
		})
	}
}

$(document).on('click','.select-reciver',function(){
	let id = $(this).data('id')
	if($(this).hasClass('bg-eoffice')) {
		$(this).removeClass('bg-eoffice')
		$(this).children('.mdi').removeClass('mdi-checkbox-marked')
		$(this).children('.mdi').addClass('mdi-checkbox-blank-outline')
	} else {
		$(this).addClass('bg-eoffice')
		$(this).children('.mdi').addClass('mdi-checkbox-marked')
		$(this).children('.mdi').removeClass('mdi-checkbox-blank-outline')
	}
})

let staging_reciver = []
$('#btn-select').click(function(){
	staging_reciver = []
	$('.select-reciver').each(function(index, value){
		if($(value).hasClass('bg-eoffice')) {
			staging_reciver.push($(value).data('id'))
		}
	})
	if(staging_reciver.length != 0) {
		$('#total-reciver').html(staging_reciver.length+' Orang')
		$('#total-reciver').removeClass('text-secondary')
	} else {
		$('#total-reciver').html('Pilih Penerima')
		$('#total-reciver').addClass('text-secondary')
	}
})
$('#modal-reciver').on('shown.bs.modal', function(e) {
	$('#search-reciver').focus()
})
$('#modal-reciver').on('hidden.bs.modal', function(e) {
	$('.select-reciver').removeClass('bg-eoffice')
	$('.select-reciver').children('.mdi').removeClass('mdi-checkbox-marked')
	$('.select-reciver').children('.mdi').addClass('mdi-checkbox-blank-outline')

	$('.select-reciver').each(function(index, value){
		let id = $(value).data('id')
		if(staging_reciver.includes(id) == true) {
			$(this).data('id',id).addClass('bg-eoffice')
			$(this).data('id',id).children('.mdi').addClass('mdi-checkbox-marked')
			$(this).data('id',id).children('.mdi').removeClass('mdi-checkbox-blank-outline')
		}
	})
	$('#search-reciver').val('')
	search('')
})