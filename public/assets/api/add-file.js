let file = []
let apiFile = []
let indexFile = 0
let fd = new FormData()

$('#add-file').click(function(){
	$(this).hide()
	indexFile = indexFile + 1
	addFile(indexFile,false)
})

$(document).on('change','.file',function(){
	let val = $(this)[0].files[0]
	let ext = val.name.split('.').pop()
	let name = convert(val.name.replace(/\.[^/.]+$/, ""))
	let name_fd = val.name.replace(/\.[^/.]+$/, "")
	let index = $(this).parent('.custom-file').data('index')
	let append = addStagingFile(val.name, ext, index)
	const format = ['jpg','jpeg','png','pdf','doc','docx','xls','xlsx','ppt','pptx']
	
	if(format.includes(ext) == true) {
		if(val.size <= 5000000) {
			$('#submit').attr('disabled',true)
			$.ajax({
				url: api_url+'file_name_cek',
				type: 'POST',
				data: {
					file_name: val.name
				},
				beforeSend: function(xhr) {
					xhr.setRequestHeader("Authorization", "Bearer "+token)
				},
				success: function(response) {
					let both = response.count
					if(response.count == 0) {
						file.push({
							file: root+'public/assets/files/'+name+'.'+ext,
							file_name: val.name
						})
						fd.append('file[]', val, val.name)
					} else {
						file.push({
							file: root+'public/assets/files/'+name+'_'+both+'.'+ext,
							file_name: val.name
						})
						fd.append('file[]', val, name_fd+'_'+both+'.'+ext)
					}
					// console.clear()
					// console.log(apiFile)
					// console.log(file)
					// console.log(...fd)
					$('#add-file').show()
					$('#submit').attr('disabled',false)
				}
			})
			$(this).parents('.custom-file').hide()
			$(this).parents('.file-group').append(append)
		} else {
			$(this).addClass('is-invalid')
			$(this).siblings('.invalid-feedback').html('Ukuran lampiran maksimal 5MB.')
		}
	} else {
		$(this).addClass('is-invalid')
		$(this).siblings('.invalid-feedback').html('Format lampiran wajib berupa JPG/PNG/PDF/DOC/XLS/PPT.')
	}
})

$(document).on('click','.mdi-staging',function(){
	// console.clear()
	let index = $(this).parent('.staging-file').data('index')
	$.each(file, function(i, v){
		if (i == index) file.splice(i, 1); i--;
	})

	var files = fd.getAll('file[]')
	files.splice(index,1)
	fd.delete('file[]')
	$.each(files, function(i, v) {
	    fd.append('file[]', v)
	})
	$(this).parents('.file-group').remove()
	$('.staging-file').each(function(i,o){
		$(this).data('index',i)
	})
	if($('.staging-file').length == 0) {
		indexFile = 0
		addFile(indexFile,true)
		$('#add-file').hide()
	}
	// console.log(apiFile)
	// console.log(file)
	// console.log(...fd)
})

$(document).on('click','.mdi-api',function(){
	// console.clear()
	let index = $(this).parent('.staging-file-api').data('index')
	$.each(apiFile, function(i, v){
		if (i == index) apiFile.splice(i, 1); i--;
	})
	$(this).parents('.file-group').remove()
	$('.staging-file-api').each(function(i,o){
		$(this).data('index',i)
	})
	// console.log(apiFile)
	// console.log(file)
	// console.log(...fd)
})

function addFile(index) {
	let append = 
	`<div class="file-group mb-3">
		<div class="custom-file" data-index="${index}">
			<input type="file" class="file custom-file-input" role="button">
			<label class="custom-file-label">Pilih Lampiran</label>
			<div class="invalid-feedback"></div>
		</div>
	</div>`
	$('#form-file').append(append)
}
function addStagingFile(name, type, index) {
	let append = 
	`<div class="staging-file d-flex align-items-center border rounded pl-2" data-index="${index}">
		<i class="mdi mdi-18px ${icon(type)}"></i>
		<div class="text-truncate">${name}</div>
		<i class="mdi mdi-close mdi-staging ml-auto pl-2 py-2" role="button"></i>
	</div>`
	return append
}
function addStagingFileApi(name, type, index) {
	let append = 
	`<div class="file-group mb-3">
		<div class="staging-file-api d-flex align-items-center border rounded pl-2" data-index="${index}">
			<i class="mdi mdi-18px ${icon(type)}"></i>
			<div class="text-truncate" title="${name}">${name}</div>
			<i class="mdi mdi-close mdi-api ml-auto pl-2 py-2" role="button"></i>
		</div>
	</div>`
	$('#form-file-api').append(append)
}

function icon(type) {
	var mdi
	switch(type) {
		case 'jpg' :
		case 'png' :
		case 'jpeg' :
			mdi = 'mdi-file-image-outline'
			break
		case 'pdf' :
			mdi = 'mdi-file-pdf-outline text-danger'
			break
		case 'doc' :
		case 'docx' :
			mdi = 'mdi-file-word-outline text-primary'
			break
		case 'xls' :
		case 'xlsx' :
			mdi = 'mdi-file-excel-outline text-success'
			break
		case 'ppt' :
		case 'pptx' :
			mdi = 'mdi-file-powerpoint-outline text-warning'
	}
	return mdi
}

function convert(text) {
	return text.replace(/\s+/g, '%20')
	// return text.replace(/[^\w ]+/g,'').replace(/ +/g,'%20')
}