let status = false
let avatar = $('#avatar-preview').croppie({
	viewport: {
		width: 150,
		height: 150,
		type: 'square'
	},
	boundary: {
		width: 200,
		height: 200
	}
})

$('#avatar').change(function(){
	$('#feedback-file').hide()
	$('.modal-footer').show()

	let file = this.files[0]
	let fileType = file['type']
	let fileSize = file['size']

	if(fileType == 'image/jpeg') {
		if(fileSize <= 1000000) {
	        if($('#avatar')[0].files.length !== 0) {
				$('#avatar-form').hide()
				$('#avatar-preview').show()
				let reader = new FileReader()
				reader.onload = function(event){
					avatar.croppie('bind',{
						url: event.target.result
					}).then(function(){
						$('#upload').attr('disabled',false)
					})
				}
				reader.readAsDataURL(this.files[0])
			}
		} else {
			reset()
			$('#feedback-file').show()
			$('#feedback-file').html('Ukuran maksimum 1MB.')
		}
	} else {
		reset()
		$('#feedback-file').show()
		$('#feedback-file').html('Format foto harus JPG.')
	}
})

$('#upload').click(function(event){
	status = true
	avatar.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function(response){
		$('#upload').attr('disabled',true)
		$('#loadAvatar').show()

		let fd = new FormData()
		let file = dataURLtoFile(response,'crop.jpg')
		fd.append('file',file)
		// console.log(...fd)

		$.ajax({
			url: root+'public/avatar.php',
			type: 'POST',
			data: fd,
			contentType: false,
			processData: false,
			success: function(newResponse) {
				// console.log(response)
				$.ajax({
					url: api_url+'user/update_foto/'+id,
					type: 'PATCH',
					beforeSend: function(xhr) {
						xhr.setRequestHeader("Authorization", "Bearer "+token)
					},
					data: {
						foto: root+'public/assets/images/profiles/'+newResponse
					},
					success: function(result) {
						let value = result.data
						$.ajax({
							url: root+'session/login',
							type: 'GET',
							data: {
								token: token,
								id: value.id,
								avatar: value.foto,
								name: value.name,
								email: value.email,
								jabatan: value.jabatan.jabatan,
								username: username,
								nip: value.nip,
								gender: value.jenis_kelamin,
								phone: value.no_hp
							},
							success: function(result) {
								status = false
								$('#loadAvatar').hide()
								$('#modalAvatar').modal('hide')
								$('.avatar').attr('src',response)
								customAlert('<i class="mdi mdi-check"></i>Foto profil berhasil diubah.')
							}
						})
					}
				})
			}
		})
	})
})

$('#modalAvatar').on('hidden.bs.modal',function(e) {
	reset()
})
$('#back').click(function(){
	reset()
})
function reset() {
	if(status == true) {
		$('#avatar-form').hide()
		$('#avatar-preview').show()
	} else {
		$('#avatar').val('')
		$('#avatar-form').show()
		$('#avatar-preview').hide()
	}
	$('.modal-footer').hide()
	$('#feedback-file').hide()
	$('#upload').attr('disabled',true)
}

function dataURLtoFile(dataurl, filename) {
	var arr = dataurl.split(','),
	mime = arr[0].match(/:(.*?);/)[1],
	bstr = atob(arr[1]), 
	n = bstr.length, 
	u8arr = new Uint8Array(n);

	while(n--){
		u8arr[n] = bstr.charCodeAt(n);
	}

	return new File([u8arr], filename, {type:mime});
}