$('#tanggal_surat').attr('max',dateNow())

$('#form').submit(function(e){
	e.preventDefault()
	$('#submit').attr('disabled',true)
	$('#text').hide();$('#load').show()

	$('#no_surat').removeClass('is-invalid')
	$('#asal_surat').removeClass('is-invalid')
	$('#tanggal_surat').removeClass('is-invalid')
	$('#jenis_surat_id-feedback').html('')
	$('#perihal').removeClass('is-invalid')
	$('#file-feedback').html('')

	let no_surat = $('#no_surat').val()
	let asal_surat = $('#asal_surat').val()
	let tanggal_surat = $('#tanggal_surat').val()
	let jenis_surat_id = $('input[type=radio][name=jenis_surat_id]:checked').val()
	let perihal = $('#perihal').val()

	$('.mdi-loader').show()
	$('.mdi-close').hide()
	$('.mdi-close').attr('role',false)

	$.ajax({
		url: api_url+'surat/create',
		type: 'POST',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			no_surat: no_surat,
			asal_surat: asal_surat,
			tanggal_surat: tanggal_surat,
			jenis_surat_id: jenis_surat_id,
			perihal: perihal,
			file
		},
		success: function(result) {
			$.ajax({
				url: root+'public/upload.php',
				type: 'POST',
				data: fd,
				contentType: false,
				processData: false,
				success: function(response) {
					setTimeout(function(){
						$('.mdi-loader').hide()
						$('.mdi-close').show()
						$('.mdi-close').addClass('mdi-check-bold')
						$('.mdi-close').addClass('text-success')
						$('.mdi-close').removeClass('mdi-staging')
						$('.mdi-close').removeClass('mdi-close')
					},1000)
				},
				complete: function() {
					setTimeout(function(){
						localStorage.setItem('send',true)
						location.href = root+'surat-masuk'
					},2500)
				}
			})
		},
		error: function(xhr){
			$('#submit').attr('disabled',false)
			$('#text').show();$('#load').hide()
			let err = JSON.parse(xhr.responseText)
			if(err.no_surat) {
				if(err.no_surat == "The no surat field is required.") {
					$('#no_surat').addClass('is-invalid')
					$('#no_surat-feedback').html('Masukkan nomor surat.')
				}
			}
			if(err.asal_surat) {
				if(err.asal_surat == "The asal surat field is required.") {
					$('#asal_surat').addClass('is-invalid')
					$('#asal_surat-feedback').html('Masukkan asal surat.')
				}
			}
			if(err.tanggal_surat) {
				if(err.tanggal_surat == "The tanggal surat field is required.") {
					$('#tanggal_surat').addClass('is-invalid')
					$('#tanggal_surat-feedback').html('Masukkan tanggal surat.')
				}
			}
			if(err.jenis_surat_id) {
				if(err.jenis_surat_id == "The jenis surat id field is required.") {
					$('#jenis_surat_id-feedback').html('Pilih jenis surat.')
				}
			}
			if(err.perihal) {
				if(err.perihal == "The perihal field is required.") {
					$('#perihal').addClass('is-invalid')
					$('#perihal-feedback').html('Masukkan perihal.')
				}
			}
			if(err.file) {
				if(err.file == "The file field is required.") {
					$('#file-feedback').html('Masukkan lampiran.')
				}
			}
			$('.mdi-loader').hide()
			$('.mdi-close').show()
			$('.mdi-close').attr('role','button')
		}
	})
})