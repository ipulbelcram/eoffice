$.ajax({
	url: api_url+'surat/detail/'+id,
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		// console.log(result)
		let value = result.data
		$('#sender').html(value.user.name)
		$('#no_agenda').html(value.no_agenda)
		$('#no_surat').html(value.no_surat)
		$('title').prepend(value.perihal)
		$('#tanggal_surat').html(fullDate(value.tanggal_surat))
		$('#jenis_surat').html(value.jenis_surat.jenis_surat)
		$('#asal_surat').html(value.asal_surat)
		$('#perihal').html(value.perihal)
		
		let foto = (value.user.foto != '') ? value.user.foto : root+'public/assets/images/photo.png'
		$('#foto').attr('src',foto)

		if(value.user.id == id_user) {
			$('#action').show()
			$('#edit').attr('href',root+'edit-surat/'+id+'/'+id_terkirim)
		}

		let total = 0
		let append,mdi,type
		$.each(value.file, function(index, value) {
			type = value.file.split('.').pop()
			switch(type) {
				case 'jpg' :
				case 'png' :
				case 'jpeg' :
					mdi = 'mdi-file-image-outline'
					break
				case 'pdf' :
					mdi = 'mdi-file-pdf-outline'
					break
				case 'doc' :
				case 'docx' :
					mdi = 'mdi-file-word-outline'
					break
				case 'xls' :
				case 'xlsx' :
					mdi = 'mdi-file-excel-outline'
					break
				case 'ppt' :
				case 'pptx' :
					mdi = 'mdi-file-powerpoint-outline'
			}
			append =
			`<div class="col-xl-4 col-md-6">
				<div class="card rounded mb-2" title="${value.file_name}">
					<div class="card-head d-flex align-middle pl-2">
						<i class="mdi ${mdi} mdi-48px"></i>
						<div class="text-truncate pr-3 pt-3">
							<p class="text-truncate mb-0">${value.file_name}</p>
							<a href="${value.file}" target="_blank" class="small">Download</a>
						</div>
					</div>
				</div>
			</div>`
			$('#file').append(append)
			total++
		})
		$('#total-file').html(total)

		let log,link
		let appendLog = ''
		let length = value.log_surat.length
		$.each(value.log_surat, function(index, value) {
			let border = (index != 0) ? 'border-right' : ''
			link = '<a href="'+root+'history/'+value.id+'">Lihat detail</a>'
			switch(value.log_surat) {
				case 'upload_surat' :
					log = 'Mengupload Surat'
					link = ''
					break
				case 'kirim_surat' :
					log = 'Mengirim Surat'
					break
				case 'edit_surat' :
					log = 'Mengedit Surat'
					link = ''
					break
				case 'kirim_disposisi' :
					log = 'Mengirim Disposisi'
					break
				case 'proses_disposisi' :
					log = 'Sedang Dikerjakan'
					link = ''
					break
				case 'finish_disposisi' :
					log = 'Selesai'
					link = ''
			}
			appendLog =
			`<div class="row">
                <div class="col-auto text-center flex-column d-sm-flex px-1">
                    <div class="m-2">
                        <i class="mdi mdi-checkbox-blank-circle mdi-18px pr-0" style="color:#dee2e6"></i>
                    </div>
                    <div class="row" style="height:60px;margin:-15px">
                        <div class="col `+border+`">&nbsp;</div>
                        <div class="col">&nbsp;</div>
                    </div>
                </div>
                <div class="col col-xl-10 pl-0" style="padding-top:11px">
                	<div class="d-flex flex-column align-items-start">
                    	<small class="text-secondary">`+value.created_at+`</small>
                    	<small>`+value.user.name+` - `+log+`. `+link+`</small>
                    </div>
                </div>
            </div>`
			$('#log-surat').prepend(appendLog)
		})
	}
})

$('#delete').click(function(){
	$(this).attr('disabled',true)
	$.ajax({
		url: api_url+'surat/delete_surat/'+id,
		type: 'DELETE',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		success: function(result){
			location.href = root+'surat-masuk'
		}
	})
})