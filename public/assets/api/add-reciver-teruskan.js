let reciver_idt = []
$.ajax({
	url: api_url+'user/user_reciver',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		let append = ''
		$.each(result.data, function(index, value) {
			append += 
			`<div class="select-reciver-t rounded p-2 mb-2" data-id="`+value.id+`" data-name="`+value.name.toLowerCase()+`" role="button">
				<i class="mdi mdi-checkbox-blank-outline mdi-18px float-left"></i>
				<div style="margin-top:2px">
					<div class="font-weight-bold text-uppercase text-truncate">`+value.name+`</div>
					<div class="small">`+value.jabatan+`</div>
				</div>
			</div>`
			reciver_idt[index] = {'id': value.id, 'name': value.name, 'jabatan': value.jabatan}
		})
		$('#reciver-t').append(append)
	}
})

$('#search-reciver-t').keyup(function(){
	let val = $(this).val()
	search_t(val)
})
function search_t(val){
	$('#empty-t').hide()
	if(val.length > 0) {
		$('.select-reciver-t').each(function(e){
			$(this).hide()
		})
		let empty = 0
		$("#reciver-t .select-reciver-t").filter(function() {
			if($(this).data('name').toLowerCase().indexOf(val) > -1){
				$(this).toggle()
			} else {
				empty++
			}
		})
		if(empty == 7) {
			$('#empty-t').show()
		}
	} else {
		$('.select-reciver-t').each(function(e){
			$(this).show()
		})
	}
}

$(document).on('click','.select-reciver-t',function(){
	let id = $(this).data('id')
	if($(this).hasClass('bg-eoffice')) {
		$(this).removeClass('bg-eoffice')
		$(this).children('.mdi').removeClass('mdi-checkbox-marked')
		$(this).children('.mdi').addClass('mdi-checkbox-blank-outline')
	} else {
		$(this).addClass('bg-eoffice')
		$(this).children('.mdi').addClass('mdi-checkbox-marked')
		$(this).children('.mdi').removeClass('mdi-checkbox-blank-outline')
	}
})

let staging_reciver_t = []
$('#btn-select-t').click(function(){
	staging_reciver_t = []
	$('.select-reciver-t').each(function(index, value){
		if($(value).hasClass('bg-eoffice')) {
			staging_reciver_t.push($(value).data('id'))
		}
	})
	if(staging_reciver_t.length != 0) {
		$('#total-reciver-t').html(staging_reciver_t.length+' Orang')
		$('#total-reciver-t').removeClass('text-secondary')
	} else {
		$('#total-reciver-t').html('Pilih Penerima')
		$('#total-reciver-t').addClass('text-secondary')
	}
})
$('#modal-reciver-t').on('shown.bs.modal', function(e) {
	$('#search-reciver-t').focus()
})
$('#modal-reciver-t').on('hidden.bs.modal', function(e) {
	$('.select-reciver-t').removeClass('bg-eoffice')
	$('.select-reciver-t').children('.mdi').removeClass('mdi-checkbox-marked')
	$('.select-reciver-t').children('.mdi').addClass('mdi-checkbox-blank-outline')

	$('.select-reciver-t').each(function(index, value){
		let id = $(value).data('id')
		if(staging_reciver_t.includes(id) == true) {
			$(this).data('id',id).addClass('bg-eoffice')
			$(this).data('id',id).children('.mdi').addClass('mdi-checkbox-marked')
			$(this).data('id',id).children('.mdi').removeClass('mdi-checkbox-blank-outline')
		}
	})
	$('#search-reciver-t').val('')
	search_t('')
})
$('#modal-reciver-t').on('show.bs.modal', function(e) {
	$('#modal-teruskan').modal('hide')
})
$('#modal-reciver-t').on('hide.bs.modal', function(e) {
	$('#modal-teruskan').modal('show')
})