$('#form').submit(function(e){
	e.preventDefault()
	$('#empty').hide()
	$('#search').blur()
	let search = $('#search').val()
	$.ajax({
		url: api_url+'surat/search_by_no_surat',
		type: 'POST',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			no_surat: search
		},
		success: function(result) {
			// console.log(result)
			let value = result.data
			if(value != '') {
				let append,photo
				$('#data').show()
				$('#data').html('')
				$.each(result.data, function(index, value) {
					if(value.surat.jenis_surat != null) {
						photo = (value.sender.foto == '') ? root+'public/assets/images/photo.png' : value.sender.foto
						append =
						`<a href="${root}surat-masuk/${value.surat.id}/${value.surat_terkirim_id}">
							<div class="box-row clearfix">
								<small class="float-right d-block d-lg-none">${shortDate(new Date(value.surat.created_at))}</small>
								<img src="${photo}" width="30" class="float-left rounded-circle mr-3">
								<div class="d-flex flex-column flex-lg-row align-items-base align-items-lg-center mt-lg-1">
									<div class="box-title text-truncate col-xl-3 pl-0">${value.sender.name}</div>
									<div class="box-desc text-truncate col-xl-5 pl-0">${value.surat.no_surat}</div>
									<div class="box-type text-truncate d-none d-lg-inline-block text-secondary col-xl-3">${value.surat.jenis_surat.jenis_surat}</div>
									<div class="d-block d-lg-none text-secondary">Jenis: ${value.surat.jenis_surat.jenis_surat}</div>
									<small class="d-none d-lg-block text-right box-date">${shortDate(new Date(value.surat.created_at))}</small>
								</div>
							</div>
						</a>`
						$('#data').append(append)
					}
				})
			} else {
				$('#data').hide()
				$('#empty').show()
			}
		}
	})
})