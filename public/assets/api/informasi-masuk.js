$.ajax({
	url: api_url+'informasi/informasi_masuk',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		// console.log(result)
		if(result.total_informasi_masuk != 0) {
			let unread,photo
			$('#head').removeClass('none-i')
			$.each(result.data, function(index, value) {
				unread = (value.status_message == 'sent') ? 'font-weight-bold' : ''
				photo = (value.sender.foto == '') ? root+'public/assets/images/photo.png' : value.sender.foto
				$('#data').append(
					appendData(
						value.surat.id,
						value.surat_terkirim_id,
						value.sender.name,
						value.surat.asal_surat,
						value.surat.no_surat,
						value.surat.created_at,
						photo,unread
					)
				)
			})
		} else {
			$('#empty').removeClass('none-i')
		}
	}
})

function appendData(id, terkirim_id, sender, asal_surat, no_surat, created_at, photo, unread) {
	let append =
	`<a href="${root}informasi-masuk/${id}/${terkirim_id}">
		<div class="box-row clearfix">
			<small class="box-date-sm ${unread}">${shortDate(new Date(created_at))}</small>
			<img src="${photo}" width="30" class="float-left rounded-circle mr-3">
			<div class="d-flex flex-column flex-xl-row align-items-base align-items-xl-center mt-xl-1">
				<div class="box-title text-truncate col-xl-3 ${unread}">${sender}</div>
				<div class="box-desc text-truncate col-xl-8 pr-0 pr-xl-3">
					<span class="${unread}">${asal_surat}</span>
					<span class="text-secondary d-none d-xl-inline"> - ${no_surat}</span>
				</div>
				<span class="text-truncate text-secondary d-block d-xl-none">${no_surat}</span>
			</div>
		</div>
	</a>`
	return append
}