$('#form').submit(function(e){
	e.preventDefault()
	$('#text').hide()
	$('#load').show()
	$('#submit').attr('disabled',true)
	$('#email').removeClass('is-invalid')
	$('#nip').removeClass('is-invalid')

	let name = $('#name').val()
	let email = $('#email').val()
	let nip = $('#nip').val()
	let gender = $('input[type=radio][name=gender]:checked').val()
	let phone = $('#phone').val()

	$.ajax({
		url: api_url+'user/update/'+id,
		type: 'PATCH',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			name: name,
			email: email,
			nip: nip,
			jenis_kelamin: gender,
			no_hp: phone
		},
		success: function(result) {
			let value = result.data
			$('.name').html(value.name)
			$('.email').html(value.email)
			$.ajax({
				url: root+'session/login',
				type: 'GET',
				data: {
					token: token,
					id: value.id,
					avatar: value.foto,
					name: value.name,
					email: value.email,
					jabatan: value.jabatan.jabatan,
					username: username,
					nip: value.nip,
					gender: value.jenis_kelamin,
					phone: value.no_hp
				},
				success: function(result) {
					$('#text').show()
					$('#load').hide()
					$('#submit').attr('disabled',false)
					customAlert('<i class="mdi mdi-check-bold"></i>Profil berhasil disimpan.')
				}
			})
		},
		error: function(xhr) {
			let err = JSON.parse(xhr.responseText)
			// console.log(err)
			if(err.email) {
				$('#email').addClass('is-invalid')
				$('#email-feedback').html("Email telah digunakan.")
			}
			if(err.nip) {
				$('#nip').addClass('is-invalid')
				$('#nip-feedback').html("NIP telah digunakan.")
			}
			$('#load').hide()
			$('#text').show()
			$('#submit').attr('disabled',false)
		}
	})
})