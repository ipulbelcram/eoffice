if(localStorage.getItem('send') == 'true') {
	customAlert('<i class="mdi mdi-check"></i>Surat berhasil terkirim.')
	localStorage.setItem('send',null)
}

suratKeluar(0)
function suratKeluar(jenis_surat_id) {
	$('#data').html('')
	$('#empty').addClass('none-i')
	if(jenis_surat_id == 0) {
		$.ajax({
			url: api_url+'surat/surat_keluar',
			type: 'GET',
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer "+token)
			},
			success: function(result) {
				// console.log(result)
				if(result.total_surat_keluar != 0) {
					let photo
					$('#head').removeClass('none-i')
					$.each(result.data, function(index, value) {
						photo = (value.sender.foto == '') ? root+'public/assets/images/photo.png' : value.sender.foto
						statusData(value.progres_status)
						$('#data').append(
							appendData(
								value.surat.id,
								value.surat_terkirim_id,
								value.sender.name,
								value.surat.asal_surat,
								value.surat.perihal,
								value.surat.jenis_surat.jenis_surat,
								value.surat.created_at,
								photo,statusData(value.progres_status)
							)
						)
					})
				} else {
					$('#empty').removeClass('none-i')
				}
			}
		})
	} else {
		$.ajax({
			url: api_url+'surat/filter_by_jenis_surat',
			type: 'POST',
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer "+token)
			},
			data: {
				jenis_surat_id: jenis_surat_id,
				type_surat: 'surat_keluar'
			},
			success: function(result) {
				// console.log(result)
				if(result.total_surat != 0) {
					let photo
					$('#head').removeClass('none-i')
					$.each(result.data, function(index, value) {
						photo = (value.sender.foto == '') ? root+'public/assets/images/photo.png' : value.sender.foto
						statusData(value.progres_status)
						$('#data').append(
							appendData(
								value.surat.id,
								value.surat_terkirim_id,
								value.sender.name,
								value.surat.asal_surat,
								value.surat.perihal,
								value.surat.jenis_surat.jenis_surat,
								value.surat.created_at,
								photo,statusData(value.progres_status)
							)
						)
					})
				} else {
					$('#empty').removeClass('none-i')
				}
			}
		})
	}
}

function appendData(id, id_terkirim, sender, asal_surat, perihal, jenis_surat, created_at, photo, status) {
	let append =
	`<a href="${root}surat-keluar/${id}/${id_terkirim}">
		<div class="box-row clearfix">
			<small class="box-date-sm">${shortDate(new Date(created_at))}</small>
			<img src="${photo}" width="30" class="float-left rounded-circle mr-3">
			<div class="d-flex flex-column flex-xl-row align-items-base align-items-xl-center mt-xl-1">
				<div class="box-title d-flex align-items-center text-truncate col-xl-3">
					<i class="mdi mdi-status mdi-18px ${status}"></i>${sender}
				</div>
				<div class="box-desc text-truncate col-xl-5 pr-0 pr-xl-3">
					<span>${asal_surat}</span>
					<span class="text-secondary d-none d-xl-inline"> - ${perihal}</span>
				</div>
				<span class="text-truncate text-secondary d-block d-xl-none">${perihal}</span>
				<div class="box-type text-truncate d-none d-xl-inline-block text-secondary col-xl-3">${jenis_surat}</div>
				<small class="font-italic d-block d-xl-none text-secondary">${jenis_surat}</small>
			</div>
		</div>
	</a>`
	return append
}

function statusData(result) {
	let status
	switch(result) {
		case 'kirim_disposisi' :
			status = 'mdi-alpha-d-box text-primary d-inline'
			break
		case 'teruskan_disposisi' :
			status = 'mdi-alpha-t-box text-warning d-inline'
			break
		case 'sent' :
		case 'finish' :
		case 'progres' :
		case 'complete' :
			status = 'd-none'
	}
	return status
}

$('.dropdown-filter').click(function(){
	let id = $(this).data('id')
	$('.dropdown-item').removeClass('active')
	$(this).addClass('active')
	suratKeluar(id)
})