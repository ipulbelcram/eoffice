$.ajax({
	url: api_url+'surat/detail/'+id,
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		// console.log(result)
		let value = result.data
		$('#sender').html(value.user.name)
		$('#no_agenda').html(value.no_agenda)
		$('#no_surat').html(value.no_surat)
		$('title').prepend(value.perihal)
		$('#tanggal_surat').html(fullDate(value.tanggal_surat))
		$('#jenis_surat').html(value.jenis_surat.jenis_surat)
		$('#asal_surat').html(value.asal_surat)
		$('#perihal').html(value.perihal)
		
		let foto = (value.user.foto != '') ? value.user.foto : root+'public/assets/images/photo.png'
		$('#foto').attr('src',foto)

		if(value.user.id == id_user) {
			$('#action').show()
			$('#edit').attr('href',root+'edit-surat/'+id+'/'+id_terkirim)
		}

		let total = 0
		let append,mdi,type
		$.each(value.file, function(index, value) {
			type = value.file.split('.').pop()
			switch(type) {
				case 'jpg' :
				case 'png' :
				case 'jpeg' :
					mdi = 'mdi-file-image-outline'
					break
				case 'pdf' :
					mdi = 'mdi-file-pdf-outline'
					break
				case 'doc' :
				case 'docx' :
					mdi = 'mdi-file-word-outline'
					break
				case 'xls' :
				case 'xlsx' :
					mdi = 'mdi-file-excel-outline'
					break
				case 'ppt' :
				case 'pptx' :
					mdi = 'mdi-file-powerpoint-outline'
			}
			append =
			`<div class="col-xl-4 col-md-6">
				<div class="card rounded mb-2" title="${value.file_name}">
					<div class="card-head d-flex align-middle pl-2">
						<i class="mdi ${mdi} mdi-48px"></i>
						<div class="text-truncate pr-3 pt-3">
							<p class="text-truncate mb-0">${value.file_name}</p>
							<a href="${value.file}" target="_blank" class="small">Download</a>
						</div>
					</div>
				</div>
			</div>`
			$('#file').append(append)
			total++
		})
		$('#total-file').html(total)

		let log,link
		let appendLog = ''
		let length = value.log_surat.length
		$.each(value.log_surat, function(index, value) {
			let border = (index != 0) ? 'border-right' : ''
			link = '<a href="'+root+'history/'+value.id+'">Lihat detail</a>'
			switch(value.log_surat) {
				case 'upload_surat' :
					log = 'Mengupload Surat'
					link = ''
					break
				case 'kirim_surat' :
					log = 'Mengirim Surat'
					break
				case 'edit_surat' :
					log = 'Mengedit Surat'
					link = ''
					break
				case 'kirim_disposisi' :
					log = 'Mengirim Disposisi'
					break
				case 'proses_disposisi' :
					log = 'Sedang Dikerjakan'
					link = ''
					break
				case 'finish_disposisi' :
					log = 'Selesai'
					link = ''
			}
			appendLog =
			`<div class="row">
                <div class="col-auto text-center flex-column d-sm-flex px-1">
                    <div class="m-2">
                        <i class="mdi mdi-checkbox-blank-circle mdi-18px pr-0" style="color:#dee2e6"></i>
                    </div>
                    <div class="row" style="height:60px;margin:-15px">
                        <div class="col `+border+`">&nbsp;</div>
                        <div class="col">&nbsp;</div>
                    </div>
                </div>
                <div class="col col-xl-10 pl-0" style="padding-top:11px">
                	<div class="d-flex flex-column align-items-start">
                    	<small class="text-secondary">`+value.created_at+`</small>
                    	<small>`+value.user.name+` - `+log+`. `+link+`</small>
                    </div>
                </div>
            </div>`
			$('#log-surat').prepend(appendLog)
		})

		$.ajax({
			url: api_url+'surat/status_message/'+id_terkirim,
			type: 'PATCH',
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer "+token)
			}
		})
	}
})

$.ajax({
	url: api_url+'surat/surat_terkirim/'+id_terkirim,
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		// console.log(result)
		switch(result.data.progres_status) {
			case 'sent' :
				$('.btn-forward').removeClass('none')
				$('.btn-disposition').removeClass('none')
				break
			case 'complete' :
				$('.btn-forward').removeClass('none')
				$('.btn-disposition').removeClass('none')
		}
	}
})

$.ajax({
	url: api_url+'param/instruksi',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		let append = ''
		$.each(result.data, function(index, value) {
			append += 
			`<div class="form-check">
				<input name="instruksi_id" class="form-check-input" type="checkbox" id="i`+index+`" value="`+value.instruksi_id+`">
				<label class="form-check-label" for="i`+index+`">`+value.instruksi+`</label>
			</div>`
		})
		$('#instruksi').append(append)
	}
})
$.ajax({
	url: api_url+'param/sifat_disposisi',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		let append = ''
		$.each(result.data, function(index, value) {
			append = 
			`<div class="form-check">
				<input name="sifat_id" class="form-check-input" type="radio" id="`+value.sifat_diposisi+`" value="`+value.id+`">
				<label class="form-check-label" for="`+value.sifat_diposisi+`">`+value.sifat_diposisi+`</label>
			</div>`
			if(index < 3) {
				$('#sifat1').append(append)
			} else {
				$('#sifat2').append(append)
			}
		})
	}
})

$('#teruskan').submit(function(e){
	e.preventDefault()
	$('#submit-t').attr('disabled',true)
	$('#load-t').show()
	
	let keteranganTeruskan = $('#keteranganTeruskan').val()

	$.ajax({
		url: api_url+'surat/kirim_surat/'+id,
		type: 'POST',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			reciver_id: staging_reciver_t,
			keterangan: keteranganTeruskan
		},
		success: function(result) {
			// console.log(result)
			$('#submit-t').attr('disabled',false)
			$('#load-t').hide()

			$('#keteranganTeruskan').val('')
			
			staging_reciver_t = []
			$('#total-reciver-t').html('Pilih Penerima')
			$('.select-reciver-t').removeClass('bg-eoffice')
			$('.select-reciver-t').children('.mdi').removeClass('mdi-checkbox-marked')
			$('.select-reciver-t').children('.mdi').addClass('mdi-checkbox-blank-outline')

			$('#modal-teruskan').modal('hide')
			customAlert('<i class="mdi mdi-check-bold"></i>Surat berhasil diteruskan.')
		},
		error: function(xhr, ajaxOptions, thrownError){
			console.log('error upload : '+xhr.responseText)
		}
	})
})

$('#disposisi').submit(function(e){
	e.preventDefault()
	$('#submit').attr('disabled',true)
	$('#load').show()

	let instruksi_id = []
	$('input[name=instruksi_id]:checked').each(function() {
		instruksi_id.push($(this).val())
	})
	let sifat_id = $('input[type=radio][name=sifat_id]:checked').val()
	let keterangan = $('#keterangan').val()

	$.ajax({
		url: api_url+'disposisi/kirim_disposisi/'+id+'/'+id_terkirim,
		type: 'POST',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			reciver_id: staging_reciver,
			instruksi_id,
			sifat_id: sifat_id,
			keterangan: keterangan
		},
		success: function(result) {
			// console.log(result)
			$('#submit').attr('disabled',false)
			$('#load').hide()

			$('input:radio').prop('checked',false)
			$('input:checkbox').prop('checked',false)
			$('#keterangan').val('')
			
			staging_reciver = []
			$('#total-reciver').html('Pilih Penerima')
			$('.select-reciver').removeClass('bg-eoffice')
			$('.select-reciver').children('.mdi').removeClass('mdi-checkbox-marked')
			$('.select-reciver').children('.mdi').addClass('mdi-checkbox-blank-outline')
			
			$('#modal-disposisi').modal('hide')
			customAlert('<i class="mdi mdi-check-bold"></i>Surat berhasil didisposisikan.')
		},
		error: function(xhr, ajaxOptions, thrownError){
			console.log('error upload : '+xhr.responseText)
		}
	})
})

$('#delete').click(function(){
	$(this).attr('disabled',true)
	$.ajax({
		url: api_url+'surat/delete_surat/'+id,
		type: 'DELETE',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		success: function(result){
			location.href = root+'surat-masuk'
		}
	})
})