$('#tanggal_surat').attr('max',dateNow())

$('#form').submit(function(e){
	e.preventDefault()
	$('#submit').attr('disabled',true)
	$('#text').hide();$('#load').show()

	$('#no_surat').removeClass('is-invalid')
	$('#asal_surat').removeClass('is-invalid')
	$('#tanggal_surat').removeClass('is-invalid')
	$('.reciver').removeClass('is-invalid')
	$('#file-feedback').html('')

	let no_surat = $('#no_surat').val()
	let asal_surat = $('#asal_surat').val()
	let tanggal_surat = $('#tanggal_surat').val()

	$('.mdi-loader').show()
	$('.mdi-close').hide()
	$('.mdi-close').attr('role',false)

	$.ajax({
		url: api_url+'informasi/create',
		type: 'POST',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			no_surat: no_surat,
			asal_surat: asal_surat,
			tanggal_surat: tanggal_surat,
			reciver_id: staging_reciver,
			file
		},
		success: function(result) {
			$.ajax({
				url: root+'public/upload.php',
				type: 'POST',
				data: fd,
				contentType: false,
				processData: false,
				success: function(response) {
					setTimeout(function(){
						$('.mdi-loader').hide()
						$('.mdi-close').show()
						$('.mdi-close').addClass('mdi-check-bold')
						$('.mdi-close').addClass('text-success')
						$('.mdi-close').removeClass('mdi-staging')
						$('.mdi-close').removeClass('mdi-close')
					},1000)
				},
				complete: function() {
					setTimeout(function(){
						localStorage.setItem('send',true)
						location.href = root+'informasi-keluar'
					},2500)
				}
			})
		},
		error: function(xhr) {
			$('#submit').attr('disabled',false)
			$('#text').show();$('#load').hide()
			let err = JSON.parse(xhr.responseText)
			if(err.no_surat) {
				if(err.no_surat == "The no surat field is required.") {
					$('#no_surat').addClass('is-invalid')
					$('#no_surat-feedback').html('Masukkan nomor informasi.')
				}
				else if(err.no_surat == "The no surat has already been taken.") {
					$('#no_surat').addClass('is-invalid')
					$('#no_surat-feedback').html('Nomor informasi telah digunakan.')
				}	
			}
			if(err.asal_surat) {
				if(err.asal_surat == "The asal surat field is required.") {
					$('#asal_surat').addClass('is-invalid')
					$('#asal_surat-feedback').html('Masukkan asal informasi.')
				}
			}
			if(err.tanggal_surat) {
				if(err.tanggal_surat == "The tanggal surat field is required.") {
					$('#tanggal_surat').addClass('is-invalid')
					$('#tanggal_surat-feedback').html('Masukkan tanggal informasi.')
				}
			}
			if(err.reciver_id) {
				if(err.reciver_id == "The reciver id field is required.") {
					$('.reciver').addClass('is-invalid')
					$('#reciver-feedback').html('Pilih penerima.')
				}
			}
			if(err.file) {
				if(err.file == "The file field is required.") {
					$('#file-feedback').html('Masukkan lampiran.')
				}
			}
			$('.mdi-loader').hide()
			$('.mdi-close').show()
			$('.mdi-close').attr('role','button')
		}
	})
})