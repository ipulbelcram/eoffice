indexFile = -1

$.ajax({
	url: api_url+'surat/detail/'+id,
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		// console.log(result)
		let value = result.data
		$('#submit').attr('disabled',false)
		$('#no_surat').val(value.no_surat)
		$('#asal_surat').val(value.asal_surat)
		$('#tanggal_surat').val(value.tanggal_surat)
		$('#perihal').val(value.perihal)
		if(value.jenis_surat.id == 1) {
			$('#memorandum').attr('checked',true)
		} else if(value.jenis_surat.id == 2) {
			$('#permohonan').attr('checked',true)
		} else if(value.jenis_surat.id == 3) {
			$('#undangan').attr('checked',true)
		} else if(value.jenis_surat.id == 4) {
			$('#proposal').attr('checked',true)
		} else if(value.jenis_surat.id == 5) {
			$('#surat').attr('checked',true)
		}

		let ext
		$.each(value.file, function(index, value){
			ext = value.file.split('.').pop()
			apiFile.push({
				file: value.file,
				file_name: value.file_name
			})
			addStagingFileApi(value.file_name, ext, index)
		})
		// console.log(apiFile)
		// console.log(file)
		$('#add-file').show()
	}
})

$('#form').submit(function(e){
	e.preventDefault()
	$('#submit').attr('disabled',true)
	$('#text').hide();$('#load').show()

	$('#no_surat').removeClass('is-invalid')
	$('#asal_surat').removeClass('is-invalid')
	$('#tanggal_surat').removeClass('is-invalid')
	$('#jenis_surat_id-feedback').html('')
	$('#perihal').removeClass('is-invalid')
	$('#file-feedback').html('')

	let no_surat = $('#no_surat').val()
	let asal_surat = $('#asal_surat').val()
	let tanggal_surat = $('#tanggal_surat').val()
	let jenis_surat_id = $('input[type=radio][name=jenis_surat_id]:checked').val()
	let perihal = $('#perihal').val()

	let final = apiFile.concat(file)
	// console.clear()
	// console.log(final)
	// console.log(...fd)

	$.ajax({
		url: api_url+'surat/edit_surat/'+id,
		type: 'PATCH',
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer "+token)
		},
		data: {
			no_surat: no_surat,
			asal_surat: asal_surat,
			tanggal_surat: tanggal_surat,
			jenis_surat_id: jenis_surat_id,
			perihal: perihal,
			file: final
		},
		success: function(result) {
			$.ajax({
				url: root+'public/upload.php',
				type: 'POST',
				data: fd,
				contentType: false,
				processData: false,
				complete: function() {
					location.href = root+'surat-masuk/'+id+'/'+id_terkirim
				}
			})
		},
		error: function(xhr){
			$('#submit').attr('disabled',false)
			$('#text').show();$('#load').hide()
			let err = JSON.parse(xhr.responseText)
			if(err.no_surat) {
				if(err.no_surat == "The no surat field is required.") {
					$('#no_surat').addClass('is-invalid')
					$('#no_surat-feedback').html('Masukkan nomor surat.')
				}
			}
			if(err.asal_surat) {
				if(err.asal_surat == "The asal surat field is required.") {
					$('#asal_surat').addClass('is-invalid')
					$('#asal_surat-feedback').html('Masukkan asal surat.')
				}
			}
			if(err.tanggal_surat) {
				if(err.tanggal_surat == "The tanggal surat field is required.") {
					$('#tanggal_surat').addClass('is-invalid')
					$('#tanggal_surat-feedback').html('Masukkan tanggal surat.')
				}
			}
			if(err.jenis_surat_id) {
				if(err.jenis_surat_id == "The jenis surat id field is required.") {
					$('#jenis_surat_id-feedback').html('Pilih jenis surat.')
				}
			}
			if(err.perihal) {
				if(err.perihal == "The perihal field is required.") {
					$('#perihal').addClass('is-invalid')
					$('#perihal-feedback').html('Masukkan perihal.')
				}
			}
			if(err.file) {
				if(err.file == "The file field is required.") {
					$('#file-feedback').html('Masukkan lampiran.')
				}
			}
		}
	})
})