$('#form').submit(function(e) {
	addLoading()
    e.preventDefault()
    const username = $('#username').val()
    const password = $('#password').val()
    $('#username').removeClass('is-invalid')
    $('#password').removeClass('is-invalid')
    $('#feedback').hide()
    $.ajax({
        url: api_url + 'auth/login',
        type: 'POST',
        data: {
            username: username,
            password: password
        },
        success: function(result) {
            const token = result.token
            $.ajax({
                url: api_url + 'auth/user',
                type: 'GET',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token)
                },
                success: function(result) {
                    let value = result.data
                    $.ajax({
                        url: root + 'session/login',
                        type: 'GET',
                        data: {
                            id: value.id,
                            token: token,
                            user_level_id: value.user_level_id
                        },
                        success: function(result) {
                            location.href = root + 'dashboard'
                        }
                    })
                }
            })
        },
        error: function(xhr) {
        	removeLoading()
            let err = JSON.parse(xhr.responseText)
            // console.log(err)
            $('#username').addClass('is-invalid')
            $('#username-feedback').html('Data tidak ditemukan.')
            $('#password').val('')
            $('#username').focus()
        }
    })
})