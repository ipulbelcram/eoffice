$.ajax({
	url: api_url+'auth/user',
	type: 'GET',
	beforeSend: function(xhr) {
		xhr.setRequestHeader("Authorization", "Bearer "+token)
	},
	success: function(result) {
		let value = result.data
		$('.name').append(value.name)
		$('.jabatan').append(value.jabatan_name)
		if(value.foto != '') {
			$('.avatar').attr('src',value.foto)
		}

		if (window.location.href.indexOf('profil') > -1) {
			$('#name').val(value.name)
			$('#email').val(value.email)
			$('#jabatan').val(value.jabatan_name)
			$('#username').val(value.username)
			$('#nip').val(value.nip)
			value.no_hp != 0 ? $('#phone').val(value.no_hp) : ''
			if(value.jenis_kelamin == "laki-laki") {
				$('#male').attr('checked',true)
			} else if(value.jenis_kelamin == "perempuan") {
				$('#female').attr('checked',true)
			}
		}
	},
	error: function(xhr) {
		if(xhr.responseText == 'Unauthorized.') {
			$.ajax({
				url: root+'session/logout',
				type: 'GET',
				success: function(){
					location.href = root
				}
			})
		}
	}
})