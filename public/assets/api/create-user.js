get_jabatan()

function get_jabatan() {
    $.ajax({
        url: api_url + 'param/jabatan',
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result)
            let append = `<option disabled selected>Pilih</option>`
            $.each(result.data, function(index, value) {
                append += `<option value="${value.id}">${value.jabatan}</option>`
            })
            $('#jabatan_id').html(append)
        },
        error: function(xhr, status) {
            setTimeout(function() {
                get_jabatan()
            }, 1000)
        }
    })
}


function get_atasan(jabatan_id) {
	$('#atasan_id').attr('disabled', true)
    $.ajax({
        url: api_url + 'user/user_atasan/' + jabatan_id,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result)
            let append = `<option disabled selected>Pilih</option>`
            $.each(result.data, function(index, value) {
                append += `<option value="${value.id}">${value.jabatan}</option>`
            })
            $('#atasan_id').html(append)
			$('#atasan_id').attr('disabled', false)
        },
        error: function(xhr, status) {
            setTimeout(function() {
                get_atasan(jabatan_id)
            }, 1000)
        }
    })
}

function get_asdep() {
	$('#asdep_id').attr('disabled', true)
    $.ajax({
        url: api_url + 'user/user_asdep',
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result)
            let append = `<option disabled selected>Pilih</option>`
            $.each(result.data, function(index, value) {
                append += `<option value="${value.id}">${value.jabatan}</option>`
            })
            $('#asdep_id').html(append)
			$('#asdep_id').attr('disabled', false)
        },
        error: function(xhr, status) {
            setTimeout(function() {
                get_asdep()
            }, 1000)
        }
    })
}

$(document).ajaxStop(function() {
    $('#form').removeClass('hide')
    $('#loading').addClass('hide')
})

$('#jabatan_id').change(function() {
	let jabatan_id = $(this).val()
	if (jabatan_id == 24) { // Deputi 
		$('#atasan_id').parents('.form-group').hide()
		$('#asdep_id').parents('.form-group').hide()
	}
	else if (jabatan_id == 25 || jabatan_id == 26) { // Asdep & Sesdep
		$('#atasan_id').parents('.form-group').show()
		$('#asdep_id').parents('.form-group').hide()
		get_atasan(jabatan_id)
	} else {
		$('#atasan_id').parents('.form-group').show()
		$('#asdep_id').parents('.form-group').show()
		get_atasan(jabatan_id)
		get_asdep()
	}
})

$('#form').submit(function(e) {
    addLoading()
    e.preventDefault()
    $('.password').removeClass('invalid')
    $('.is-invalid').removeClass('is-invalid')
    $('#gender-feedback').hide()

    let name = $('#name').val()
    let email = $('#email').val()
    let nip = $('#nip').val()
    let gender = $('input[type=radio][name=gender]:checked').val()

    let jabatan_id = $('#jabatan_id').val()
    let jabatan_name = $('#jabatan_id').data('name')
    let atasan_id = $('#atasan_id').val()
    let asdep_id = $('#asdep_id').val()

    let username = $('#username').val()
    let npassword = $('#npassword').val()
    let cpassword = $('#cpassword').val()

    $.ajax({
        url: api_url + 'user/create',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        data: {
            name: name,
            email: email,
            nip: nip,
            jenis_kelamin: gender,
            jabatan_id: jabatan_id,
            jabatan_name: jabatan_name,
            atasan_id: atasan_id,
            asdep_id: asdep_id,
            user_level_id: 101,
            username: username,
            password: npassword,
            password_confirmation: cpassword
        },
        success: function(result) {
            location.href = root + 'user'
        },
        error: function(xhr) {
            removeLoading()
            let err = JSON.parse(xhr.responseText)
            // console.clear()
            // console.log(err)
            if (err.name) {
                $('#name').addClass('is-invalid')
                $('#name-feedback').html("Masukkan nama lengkap.")
            }
            if (err.email) {
                if (err.email == "The email field is required.") {
                    $('#email').addClass('is-invalid')
                    $('#email-feedback').html("Masukkan email.")
                } else if (err.email == "The email has already been taken.") {
                    $('#email').addClass('is-invalid')
                    $('#email-feedback').html("Email telah digunakan.")
                }
            }
            if (err.nip) {
                if (err.nip == "The nip field is required.") {
                    $('#nip').addClass('is-invalid')
                    $('#nip-feedback').html("Masukkan NIP.")
                } else if (err.nip == "The nip has already been taken.") {
                    $('#nip').addClass('is-invalid')
                    $('#nip-feedback').html("NIP telah digunakan.")
                }
            }
            if (err.jenis_kelamin) {
                $('#gender-feedback').show()
                $('#gender-feedback').html("Pilih jenis kelamin.")
            }

            if (err.jabatan_id) {
                $('#jabatan_id').addClass('is-invalid')
                $('#jabatan_id-feedback').html('Pilih jabatan.')
            }
            if (err.jabatan_name) {
                $('#jabatan_name').addClass('is-invalid')
                $('#jabatan_name-feedback').html('Masukkan nama jabatan.')
            }
            if (err.atasan_id) {
                $('#atasan_id').addClass('is-invalid')
                $('#atasan_id-feedback').html('Pilih atasan.')
            }
            if (err.asdep_id) {
                $('#asdep_id').addClass('is-invalid')
                $('#asdep_id-feedback').html('Pilih asdep.')
            }

            if (err.username) {
                if (err.username == "The username field is required.") {
                    $('#username').addClass('is-invalid')
                    $('#username-feedback').html('Masukkan username.')
                }
                if (err.username == "The username has already been taken.") {
                    $('#username').addClass('is-invalid')
                    $('#username-feedback').html('Username telah digunakan.')
                }
            }
            if (err.password || err.password_confirmation) {
                $('.password').addClass('invalid')
            }
            if (err.password) {
                if (err.password == 'The password field is required.') {
                    $('#npassword').addClass('is-invalid')
                    $('#npassword-feedback').html('Masukkan password.')
                } else if (err.password == 'The password must be at least 8 characters.') {
                    $('#npassword').addClass('is-invalid')
                    $('#npassword-feedback').html('Password minimal 8 karakter.')
                } else if (err.password == 'The password confirmation does not match.') {
                    $('#cpassword').addClass('is-invalid')
                    $('#cpassword-feedback').html('Konfirmasi password dengan benar.')
                }
            }
            if (err.password_confirmation) {
                if (err.password_confirmation == 'The password confirmation field is required.') {
                    $('#cpassword').addClass('is-invalid')
                    $('#cpassword-feedback').html('Masukkan konfirmasi password.')
                }
            }
        }
    })
})