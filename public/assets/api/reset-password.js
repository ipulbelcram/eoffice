$('#reset-password').submit(function(e) {
    e.preventDefault()

    $('.is-invalid').removeClass('is-invalid')
    $('.password').removeClass('invalid')
    $('#submit-password').attr('disabled', true)
    $('#load-password').show()
    $('#text-password').hide()

    let npassword = $('#npassword').val()
    let cpassword = $('#cpassword').val()

    $.ajax({
        url: api_url + 'user/reset_password',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        data: {
        	user_id: id,
            password: npassword,
            password_confirmation: cpassword
        },
        success: function(result) {
            $('#npassword').val('')
            $('#cpassword').val('')
		    $('.password').removeClass('invalid')
            customAlert('<i class="mdi mdi-check-bold"></i>Reset Password berhasil.')
        },
        error: function(xhr) {
            let err = JSON.parse(xhr.responseText)
            // console.clear()
            // console.log(err)
            $('.password').addClass('invalid')
            if (err.password) {
                if (err.password == 'The password field is required.') {
                    $('#npassword').addClass('is-invalid')
                    $('#npassword-feedback').html('Masukkan password baru.')
                } else if (err.password == 'The password must be at least 8 characters.') {
                    $('#npassword').addClass('is-invalid')
                    $('#npassword-feedback').html('Password minimal 8 karakter.')
                } else if (err.password == 'The password confirmation does not match.') {
                    $('#cpassword').addClass('is-invalid')
                    $('#cpassword-feedback').html('Konfirmasi password dengan benar.')
                }
            }
            if (err.password_confirmation) {
                if (err.password_confirmation == 'The password confirmation field is required.') {
                    $('#cpassword').addClass('is-invalid')
                    $('#cpassword-feedback').html('Masukkan konfirmasi password.')
                }
            }
        },
        complete: function() {
		    $('#submit-password').attr('disabled', false)
		    $('#load-password').hide()
		    $('#text-password').show()
        }
    })
})