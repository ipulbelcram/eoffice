<?php
	function random($length) {
		$char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$string = "";
		for($i=1;$i<=$length;$i++) {
			$pos = rand(0, strlen($char)-1);
			$string .= $char{$pos};
		}
		return $string;
	}

	$filename = $_FILES["file"]["name"];
	$extension = pathinfo($filename,PATHINFO_EXTENSION);
	$newfileName = random(24).".".$extension;

	$path = "assets/images/profiles/".$newfileName;
	$imageFileType = pathinfo($path,PATHINFO_EXTENSION);
	$success = true;

	$valid_extensions = array("jpeg","jpg","png","pdf");
	if(!in_array(strtolower($imageFileType), $valid_extensions)) {
		$success = false;
	}

	if($success == true){
		if(move_uploaded_file($_FILES["file"]["tmp_name"],$path)){
			echo $newfileName;
		} else {
			echo "Cannot upload file";
		}
	} else {
		echo "Invalid extension";
	}
?>